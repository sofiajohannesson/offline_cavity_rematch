#!/usr/bin/env python
# coding: utf-8

# # Library of functions for Cavity rematch

# # variables

# In[49]:


#imports
import numpy as np
import sys
import constants as cnst
import twissFunc

#natural contants
#c = 2.99792458e8 #m/s, speed of light
#m0 = 938.272029 #MeV, proton mass


# # functions

# In[50]:


def trapz(y,x):
    '''Approximate the integral of f where x is the integration points.

    The trapezoid rule approximates the integral \int_a^b y(x) dx by the sum:
    (dx/2) \sum_{k=1}^N (y(x_k) + y(x_{k-1}))
    where x_k = a + k*dx and dx = (b - a)/N.

    Input:
        - y : function values at points x
        - x : domain of definition for y

    Output:
     - float: Approximation of the integral of y over x using the
        trapezoid rule with N subintervals of equal length.
    '''
    
    a = x[0]
    b = x[-1]
    N = len(x)
    y_right = y[1:] # Right endpoints
    y_left = y[:-1] # Left endpoints
    dx = (b - a)/N
    T = (dx/2) * np.sum(y_right + y_left)
    return T


# In[51]:


def readLattice(latticeName):
    '''read lattice file and turn to a matrix with four columns
     
    Input:
         - lattice file with all components of the accelerator as lines with the parameters
             specified in the format used in TraceWin
    
    Output:
         - the name of a simplified lattice file with only five types of components.
             This file contains four columns specifed as:
             - Drift: 4, length, 0, 0
             - Quad:  5, length, strength/gradient, 0
             - SPK:   1, length, AMP, Input_Phase
             - MBL:   2, length, AMP, Input_Phase
             - HBL:   3, length, AMP, Input_Phase
             
             (All parameters extracted from the original lattice file)
    
    
    '''
    
    simpleLattice = 'simpleLattice.txt'
    
    #clear file if it already exists
    simLt = open('simpleLattice.txt', 'w').close()
    
    try:
        lt = open(latticeName, "r")
        simLt = open(simpleLattice, "w")
        
        #create header
        simLt.write('; drift: 4, length, 0, 0 \n; quad: 5, length, strength, 0 \n; SPK, MBL or HBL: (1,2 or 3), length, AMP, Input_Phase \n')
        
        tempLine = lt.readline()
        lastLineBool = ('end' in tempLine)
        
        #To deal with error in lattice-file
        nbrLines = 0
        #temp = 0
        
        while(lastLineBool == False): 
            tempLine = lt.readline() #reads one line and then moves onto the next
            if((';' in tempLine)== False):
                lastLineBool = ('END' in tempLine) and (len(tempLine) ==4)
            #temp += 1
            #print(f"lastlineBool = {lastLineBool} and length of line is {len(tempLine)} at line {temp} line is: {tempLine}")
            if tempLine != "" :#check if line is empty
                
                #if nbrLines == 748:
                #    #DRIFT has 5 letters, remove the 6th character to remove the extra whitespace in lattice file
                #    tempLine = tempLine[0:5] + tempLine[7:]
                params = tempLine.split(' ')
                
                #capital/lowercase letters matter
                if  len(tempLine) > 4 and ('QUAD' in tempLine) and tempLine[4] == ' ':
                    simLt.write(str(5) + ' ' + params[1] + ' ' + params[2] + ' 0 \n')
                    nbrLines += 1
                elif len(tempLine) > 5 and ('DRIFT' in tempLine) and tempLine[5] == ' ':
                    nbrLines += 1
                    simLt.write(str(4) + ' ' + params[1] + ' 0 0 \n')
                    
                elif len(tempLine) > 9 and ('FIELD_MAP' in tempLine) and tempLine[9] == ' ':
                    params = tempLine.split(' ')
                    
                    if ('Spoke_W_coupler' in tempLine):
                        #the order of the parameters are defined in traceWin
                        simLt.write(str(1) + ' ' + params[2] + ' ' + params[6] + ' ' + params[3] + ' \n')
                        nbrLines += 1
                        
                    elif ('MB_W_coupler' in tempLine):
                        simLt.write(str(2) + ' ' + params[2] + ' ' + params[6] + ' ' + params[3] + ' \n')
                        nbrLines += 1
                        
                    elif ('HB_W_coupler' in tempLine):
                        simLt.write(str(3) + ' ' + params[2] + ' ' + params[6] + ' ' + params[3] + ' \n')
                        nbrLines += 1        
    
    finally:
        lt.close()
        simLt.close()
        
    return simpleLattice



# In[52]:


def Cavity_Matrix_Input_Phase(elementType, Input_Phase, Input_Energy, AMP, Field_Map):
    """ A function that decribes the particles movement in an accelerating cavity
    
    Input:
         - elementType: 1 = spoke, 2 = MBL and 3 = HBL
         - Input_Phase: defined by the lattice file (WHAT DOES THIS MEAN? Not defined by the abs_phase before?) [degrees]
         - Input_Energy: the energy of the particle enetring this cavity [MeV]
         - AMP: how much of the maximum power the cavity is running at, set in the lattice file.
         - Field_Map: the field_map of the relevant cavity running at the maximum power. Must be an array. [z (m), Ex (MV/m), Ey (MV/m), Ez (MV/m)]
    
    Output:
         - Phase_Shift: The phase change of the beam as a result from travveling in the cavity [degrees]
         - Output_Energy: The energy of the beam a result of travelling in the cavity [MeV]
         - M: The matrix that descibes the cavities affect on the 6D phase-space of the beam [radian-m phase-space]
    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    if elementType == 1:
        f = cnst.f #Hz
    elif elementType == 2 or elementType == 3:
        f = 2*cnst.f
    else:
        print('Not a cavity as elementType input to Cavity_Matrix_Input_Phase, it is ' + str(elementType))
    
    Ez = Field_Map[:,3] # MV/m
    Ez = Ez*AMP
    z = Field_Map[:, 0] #m
    N = len(Ez)
    L_total = z[N-1]
    L = L_total/(N-1)
    
    In_Phase = Input_Phase*2*np.pi/360 #convert phase to radians
    gamma_i = 1 + Input_Energy/m0 #m0 defined in 'variables'
    beta_i = np.sqrt(1-1/gamma_i**2)
    
    Mz = np.identity(2)
    Mx = np.identity(2)
    
    Phase_Shift = 0
    
    for i in range(0,N):
        gamma_o = gamma_i + Ez[i]*np.cos(In_Phase)*L/m0 # don't know where this comes from
        beta_o = np.sqrt(1-1/gamma_o**2)
        
        #calculate dE (derivative of field times dL)
        if i == 0:
            dE = Ez[1]/2 #Ez is zero at the edges of the cavity
        elif i == N-1:
            dE = Ez[N-2]/2
        else:
            dE = (Ez[i+1]-Ez[i-1])/2
        
        k = 1/(gamma_i*(beta_i**2)*m0)
        
        #coefficients from matrix in longitudinal phase space (Mzz)
        
        m21zz = k*dE*np.cos(In_Phase)
        m22zz = 1 - k*Ez[i]*L*np.cos(In_Phase)
        
        #coefficients from matrix in tranversal phase space (Mxx=Myy)
        
        m21xx = k*(-0.5*dE*np.cos(In_Phase)+beta_i*c*2*np.pi*f/(2*c**2)*Ez[i]*L*np.sin(In_Phase))
        m22xx = 1-k*Ez[i]*L*np.cos(In_Phase)
        
        if i < N-1:
            
            Mz1 = [[1, L/gamma_o**2], [m21zz, m22zz]]
            
            Mx1 = [[1, L], [m21xx, m22xx]]
            
        else:
            Mz1 = np.dot([[1, L/gamma_o**2], [m21zz, m22zz]], np.linalg.inv([[1, L/gamma_o**2], [0, 1]]))
            
            Mx1 = np.dot([[1, L], [m21xx, m22xx]], np.linalg.inv([[1, L], [0, 1]]))
        
        Mz = np.dot(Mz1,Mz)
        Mx = np.dot(Mx1,Mx)
        
        d_Phy = 2*np.pi*f*L/(beta_o*c)
        In_Phase = In_Phase + d_Phy
        
        if i < N-1:
            Phase_Shift = Phase_Shift+d_Phy
        
        gamma_i = gamma_o
        beta_i = beta_o
    
    Output_Energy = (gamma_o-1)*m0 
    
    #convert Phase_shift to degrees
    if elementType == 1:
        Phase_Shift = Phase_Shift*360/(2*np.pi)
    else: #already checked to that elementType was 1,2 or 3, can therefore use only "else"
        Phase_Shift = Phase_Shift*360/(2*np.pi)/2
        
    My = Mx
    
    M0 = np.zeros((2,2))
    #M = np.array([[Mx, M0, M0], [M0, My, M0], [M0, M0, Mz]])
    M1 = np.concatenate((Mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, My, M0), axis = 1)
    M3 = np.concatenate((M0, M0, Mz), axis = 1)
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    return Phase_Shift, Output_Energy, M


# In[53]:


def Input_to_Sync(elementType, Input_Phase, Input_Energy, AMP, Field_Map):
    """ Calculated the synchronous phase of a cavity using the input phase and the input energy.
    
    Input:
         - elementType: Type of element (1 = SPK, 2 = MBL or 3 = HBL)
         - Input_Phase: defined by the lattice file
         - Input_Energy: the energy of the particle enetring this cavity [MeV]
         - AMP: how much of the maximum power the cavity is running at, set in the lattice file.
         - Field_Map: the field_map of the relevant cavity running at the maximum power. Must be an array. [z (m), Ex (MV/m), Ey (MV/m), Ez (MV/m)]
    
    Output:
          - Sync_Phase: the synchonous phase of a cavity    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    if elementType == 1:
        f = cnst.f
    elif elementType == 2 or elementType == 3:
        f = 2*cnst.f
    else:
        print('Wrong elemenType parameter in Input_to_Sync')
        
    Ez = Field_Map[:,3] # MV/m
    Ez = Ez*AMP
    z = Field_Map[:, 0] #m
    
    wave = c/f
    
    N = len(Ez)
    L_total = z[N-1]
    L = L_total/(N-1)
    
    gamma_i = Input_Energy/m0+1
    In_Phase = Input_Phase*2*np.pi/360 # convert Input_Phase to radians
    phiss = np.zeros(N)
    
    for i in range(0,N-1):
        phiss[i] = In_Phase
        
        gamma_o = gamma_i + Ez[i]*np.cos(In_Phase)*L/m0
        beta_o = np.sqrt(1-1/gamma_o**2)
        
        d_Phy = 2*np.pi*f*L/(beta_o*c)
        In_Phase = In_Phase + d_Phy
        gamma_i = gamma_o
        
    avec = np.multiply(Ez, np.sin(phiss))
    bvec = np.multiply(Ez, np.cos(phiss))
    a = trapz(avec, z)
    b = trapz(bvec, z)
    Sync_Phase = np.math.atan(a/b)
    Sync_Phase = Sync_Phase*360/(np.pi*2) #convert to degrees
    
    return Sync_Phase


# In[54]:


def Ref_Phase(simpleLatticeName, Input_Energy_0, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL):
    """ Calculate the ref-phase for all elements in the simplified latttice of the accelerator
    
    Input:
        - simplified lattice file with four colums as. Type, length, parameter, parameter.
        
    Output:
        - The name of a txt-file containing data about all accelteretor components in 10 columns:
            Type
            Length (mm)
            gradient (T/m) or AMP (factor of power stored in Field Map)
            Input_Phase (degree)
            Sync_Phase (degree)
            Input_energy (MeV)
            Output_energy (MeV)
            d_Phy (degree)
            Full_Length (m)
            abs_Phase (degree)
            
            If a parameter is not relevant for a component it will be given the value NaN
    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    #converting the textfile to a matrix (if the txt file already is created, only this needs to be done)
    
    latticeMatrix = np.loadtxt(simpleLatticeName, delimiter = ' ', usecols = (0,1,2,3), comments = ';')
    
    Input_Energy = Input_Energy_0
    N = len(latticeMatrix[:,0])#number of element in latticeMatrix
    phaseFileName = 'Ref_Phase.txt'
    
    phaseFile = open('Ref_Phase.txt', 'w').close()
    #save a list of paramenetrs for all elements
    #type, length(m), gradient(T/m), Input_Phase, Sync_Phase, Input_energy, Output_energy, d_Phy, Full_Length(m), abs_Phase
    #if an element does not have a value for one (or more) of these parameters, the value is assigned 'NaN'
    
    with open(phaseFileName, 'w') as phaseFile:
        for i in range(0,N):
            print(f"in ref_phase i = {i}")
            if i == 0:
                #write a header in the file before writing any parameters
                abs_Phase = 0
                Output_Energy = Input_Energy
                z = 0
                phaseFile.write('# Type, Length(mm), gradient(T/m) or AMP, Input_Phase(degree), Sync_Phase(degree), Input_energy(MeV), Output_energy(MeV), d_Phy(degree), Full_Length(m), abs_Phase(degree) \n')
                
            else:
                Input_Energy = Output_Energy
                
            typeElement = latticeMatrix[i,0]
            
            if typeElement == 4: # drift
                f = cnst.f #Hz ???????????????????????????????? is this true for all drifts, even the ones after the superconducting part?
                L = latticeMatrix[i,1]/1000 #in units of m
                z = z+L
                #d_Phy = 360*f*L/(beta*c)
                d_Phy, Output_Energy,M = Drift_Matrix(Input_Energy,L)
                abs_Phase = abs_Phase + d_Phy
                phaseFile.write(f"{typeElement} {L*1000} NaN NaN NaN {Input_Energy} {Output_Energy} {d_Phy} {z} {abs_Phase} \n")
                
            
            elif typeElement == 5: #quad
                f = cnst.f # Hz
                #Input_Energy = Output_Energy
                #gamma = 1 + Input_Energy/m0
                #beta = np.sqrt(1-1/(gamma**2))
                L = latticeMatrix[i,1]/1000 # to units of m
                z = z+L
                #d_Phy = 360*f*L/(beta*c)
                G = latticeMatrix[i,2]
                d_Phy, Output_Energy, M = Quad_Matrix(Input_Energy,L,G)
                abs_Phase = abs_Phase + d_Phy
                phaseFile.write(f"{typeElement} {L*1000} {G} NaN NaN {Input_Energy} {Output_Energy} {d_Phy} {z} {abs_Phase} \n")
                
                
            elif typeElement == 1 or typeElement == 2 or typeElement == 3:
                AMP = latticeMatrix[i,2]
                Input_Phase = latticeMatrix[i,3]
                #Input_Energy = Output_Energy #the energy the particle has coming out from the previous element
                L = latticeMatrix[i,1]/1000 #convert to m
                z = z+L
                
                if typeElement ==1:
                    Field_Map = FIELD_MAP_SPK
                elif typeElement ==2:
                    Field_Map = FIELD_MAP_MBL
                else:
                    Field_Map = FIELD_MAP_HBL
                
                #using the matrix of a accelerating cavity
                Phase_Shift, Output_Energy, M = Cavity_Matrix_Input_Phase(typeElement, Input_Phase,
                                                                          Input_Energy, AMP, Field_Map) 
                d_Phy = Phase_Shift
                abs_Phase = abs_Phase + d_Phy
                Sync_Phase = Input_to_Sync(typeElement, Input_Phase, Input_Energy, AMP, Field_Map)
                
                phaseFile.write(f"{typeElement} {L*1000} {latticeMatrix[i,2]} {Input_Phase} {Sync_Phase} {Input_Energy} {Output_Energy} {d_Phy} {z} {abs_Phase} \n")
                
    phaseFile.closed
    
    return phaseFileName

def Ref_Phase2(simpleLatticeName, Input_Energy_0, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL):
    """ Calculate the ref-phase for all elements in the simplified latttice of the accelerator
    
    Input:
        - simplified lattice file with four colums as. Type, length, parameter, parameter.
        
    Output:
        - return a matrix with information of the lattice in 10 columns:
            Type
            Length (mm)
            gradient (T/m) or AMP (factor of power stored in Field Map)
            Input_Phase (degree)
            Sync_Phase (degree)
            Input_energy (MeV)
            Output_energy (MeV)
            d_Phy (degree)
            Full_Length (m)
            abs_Phase (degree)
            
            If a parameter is not relevant for a component it will be given the value NaN
    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    #converting the textfile to a matrix (if the txt file already is created, only this needs to be done)
    
    latticeMatrix = np.loadtxt(simpleLatticeName, delimiter = ' ', usecols = (0,1,2,3), comments = ';')
    
    Input_Energy = Input_Energy_0
    N = len(latticeMatrix[:,0])#number of element in latticeMatrix

    # create a phase array containing these columns:
    # type, length(m), gradient(T/m), Input_Phase, Sync_Phase, Input_energy, Output_energy, d_Phy, Full_Length(m), abs_Phase
    # if an element does not have a value for one (or more) of these parameters, the value is assigned 'NaN'
    phases = np.zeros((N, 10))
    phases[:] = np.nan
    for i in range(0,N):
        #print(f"in ref_phase i = {i}")
        if i == 0:
            abs_Phase = 0
            Output_Energy = Input_Energy
            z = 0
        else:
            Input_Energy = Output_Energy

        typeElement = latticeMatrix[i, 0]
        phases[i, 0] = typeElement

        if typeElement == 4: # drift
            f = cnst.f
            L = latticeMatrix[i, 1]/1000 # in units of m
            z = z + L
            d_Phy, Output_Energy, M = Drift_Matrix(Input_Energy,L)
            abs_Phase = abs_Phase + d_Phy
        elif typeElement == 5: # quad
            f = cnst.f # Hz
            # Input_Energy = Output_Energy
            # gamma = 1 + Input_Energy/m0
            # beta = np.sqrt(1-1/(gamma**2))
            L = latticeMatrix[i,1]/1000 # to units of m
            z = z+L
            # d_Phy = 360*f*L/(beta*c)
            G = latticeMatrix[i,2]
            d_Phy, Output_Energy, M = Quad_Matrix(Input_Energy,L,G)
            abs_Phase = abs_Phase + d_Phy
            phases[i, 2] = G
        elif typeElement == 1 or typeElement == 2 or typeElement == 3:
            AMP = latticeMatrix[i, 2]
            Input_Phase = latticeMatrix[i, 3]
            # Input_Energy = Output_Energy #the energy the particle has coming out from the previous element
            L = latticeMatrix[i, 1]/1000  # convert to m
            z = z+L
            if typeElement == 1:
                Field_Map = FIELD_MAP_SPK
            elif typeElement == 2:
                Field_Map = FIELD_MAP_MBL
            else:
                Field_Map = FIELD_MAP_HBL

            # using the matrix of a accelerating cavity
            Phase_Shift, Output_Energy, M = Cavity_Matrix_Input_Phase(typeElement, Input_Phase,
                                                                      Input_Energy, AMP, Field_Map)
            d_Phy = Phase_Shift
            abs_Phase = abs_Phase + d_Phy
            Sync_Phase = Input_to_Sync(typeElement, Input_Phase, Input_Energy, AMP, Field_Map)
            phases[i, 2] = AMP
            phases[i, 3] = Input_Phase
            phases[i, 4] = Sync_Phase

        phases[i, 1] = L * 1000
        phases[i, 5:] = [Input_Energy, Output_Energy, d_Phy, z, abs_Phase]

    return phases




# In[55]:


def list_z(cavType, Num, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL, Ref_Phase):
    """ This function calculates which cavites are the four closest to the offline cavity.
    These will be adjusted in order to maintain the beam stability.
    
    Input:
        - elementType: type of cavity, either 1 (SPK), 2 (MBL) or 3 (HBL) that has failed
        - Num: The number of the failed cavity. if SPK, a number between 1-26,
                                          if MBL between 1-36 and if HBL between 1-84
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: teh field map of a HBL cavity
        - Ref_Phase: the matrix contaiaing all infromation in the previously created file ref_Phase.
                A matrix of 10 columns where each row corresponds to a compondent in the accelerator.
                The 10 columns are: Type, length (mm), gradient(T/m) or AMP, Input_Phase, Sync_Phase, 
                Input_Energy, Output_Energy, d_Phy, Full_length, Abs_Phase
                All these are expained in the function Ref_Phase
                
    Output:
        - head: number of the first element in the section in the big lattice.
        - Lattice_Section_F: The four closest caveties to the failed one are chosen for rematch. This is 
                            the lattice that contains all elements between the two outermost caveties of
                            the five chosen ones.
        - M_ref: the reference transfer matrix of the fully functioning accelerator. (Just the five chosen
                    cavities)
        - Rematch_Cavities_index: position of cavities to be rematched: column 1: type, 
                            column 2: absolute position, column 3: relative position in current section
        - margins: the upper boundary of the max power for the 4 cavities that will be used for rematch (1 is 100 % power)
    
    """
    
    #############################################################################
    #    this needs to change to take multiple offline cavities into account
    #############################################################################
    # define the closest surrounding cavities. How to deal with a offline cavity close to the border. 
    
    former = 2 #how many cavities before the offline one that shall be adjusted
    later = 2 #how many cavities after the offline one that shall be adjusted
    #if cavType == 1:
    #    if Num == 1:
    #        former = 0
    #        later = 4
    #    elif Num == 2:
    #        former = 1
    #        later = 3
    #elif cavType == 3:
    #    if Num == 83:
    #        former = 3
    #        later = 1
    #    elif Num == 84:
    #        former = 4
    #        later = 0
    
    if cavType == 1: #i.e. SPK Lattice
        if Num == 1:
            former = 0
            later = 4
        elif Num == 2:
            former = 1
            later = 3
        elif Num == 25:
            former = 3
            later = 1
        elif Num == 26:
            former = 4
            later = 0
            
    if cavType == 2: #i.e. BL lattice
        if Num == 1:
            former = 0
            later = 4
        elif Num == 2:
            former = 1
            later = 3
    elif cavType == 3:
        if Num == 83:
            former = 3
            later = 1
        elif Num == 84:
            former = 4
            later = 0
    
    #includes all cavities
    types = Ref_Phase[:,0]
    mtemp = np.where(types < 4)
    typeIdxs = mtemp[0] # indexes in Ref_Phase of all cavities
    offset = 0
    #when we had the full lattice
    #if cavType == 2:
    #    offset = 26
    #elif cavType == 3:
    #    offset = 26+36
    
    #now only half
    if cavType == 3:
        offset = 36 #since 36 MBL cavities
    row_Failed = typeIdxs[offset + Num - 1] #-1 since python Idx starts on 0
    
    #find absolute position of the section that will be used for the cavity rematch
    #less complicatde than Qins code, perhaps I missed something?
    
    ########### does not care about borader between 1-2 and 2-3, since all cavities are in typeIdxs
    head = typeIdxs[offset + Num-former-1]
    #print(f"head = {head}")
    tail = typeIdxs[offset + Num + later -1]
    Lattice_Section = Ref_Phase[head:tail+1, :] #+1 because python takes the interval [head,tail[
    
    #get the transfer matrix of the chosen section (head:tail)
    
    M_ref = Lattice_2_Matrix(Lattice_Section, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL)
    
    Lattice_Section_F = Lattice_Section.copy()
    Lattice_Section_F[row_Failed-head, 0] = 0 # setting the type to 0
    Lattice_Section_F[row_Failed-head, 2] = 0 # setting the AMP to 0
    #print(f"cavType = {cavType}")
    if cavType == 1:
        typeName = 'SPK'
    elif cavType == 2:
        typeName = 'MBL'
    elif cavType == 3:
        typeName = 'HBL'
    else:
        typeName = ' Not correct type in list_z'
    
    print('When cavity ' + typeName + '-' + str(Num) + ' fails, four cavities beside it are used for rematching: \n')
    
    #get absolute position of the five cavities
    #cavities to be rematched: column 1: type, column 2: absolute position, column 3: relative position in current section
    Rematch_Cavities_index = np.zeros((5,3))
    cavNbrs = Num + np.linspace(-former,later,5)
    cavNbrs = cavNbrs.astype(int)
    #print(f"cavNbrs = {cavNbrs}")
    j = 0
    #idxDiffs = np.linspace(-former,later,5)
    #print(f"idxDiffs = {idxDiffs}")
    nbrCavs = 0
    for i in range (0,len(Lattice_Section_F[:,0])):
        elementType = Lattice_Section_F[i, 0]
        if elementType < 4: #0, 1, 2 or 3
            Rematch_Cavities_index[j,1] = i + head # absolute position
            
            # count how many cavities of the same kind there is before in Ref_Phase
            #Rematch_Cavities_index[j,2] = sum(Ref_Phase[0:(i+head),0]==Lattice_Section[i,0])
            #Rematch_Cavities_index[j,2] = Num + idxDiffs[nbrCavs] -1 #pyhtonIdx
            #nbrCavs +=1
            Rematch_Cavities_index[j,2] = i #relative position in current section
            
            if elementType ==0:
                failedCavType = Lattice_Section[i,0]
            Rematch_Cavities_index[j,0] = elementType #if it was 0 in Lattice_Section_F it is now set to zero
            j += 1
    
    Rematch_Cavities_index = Rematch_Cavities_index.astype(int) # somewhat unnessesary but nice when printing.
    
    print("The type, index in Ref_phase and index in Lattice_Section_F of the cavities to be rematched and the failed cavity")
    print(Rematch_Cavities_index)
    if j != 5:
        print('Not correct amount of Cavities in Rematch_Cavities matrix (error in List_z), j = ' + str(j))
        
    ##### Get margins for the four cavities beside the failed one #########
    margins = [1, 1, 1, 1]
    # Margins in the fraction of top amplitude the four cavities are at before the rematching
    #The first 20 MBL cavities are not running at max power.
    j = 0 #why did it start w 1 before?
    for i in range(0, len(Rematch_Cavities_index)):
        cavityType = Rematch_Cavities_index[i,0]
        if cavityType == 1:
            #+1 cause python index start at 0
            #print(f"i = {i} and cavNbrs[i] = {cavNbrs[i]}")
            print('\033[94m' + 'SPK-' + str(cavNbrs[i]) + '\t' + '\033[0m')
            j += 1
        elif cavityType == 2:
            #print(f"i = {i} and cavNbrs[i] = {cavNbrs[i]}")
            print('\033[94m' + 'MBL-' + str(cavNbrs[i]) + '\t' + '\033[0m')
            if Num <= 20:
                margins[j] = 1/Ref_Phase[Rematch_Cavities_index[i,1], 2] #1/AMP
            j += 1
        elif cavityType == 3:
            #print(f"i = {i} and cavNbrs[i] = {cavNbrs[i]}")
            print('\033[94m' + 'HBL-' + str(cavNbrs[i]) + '\t' + '\033[0m')
            j += 1
        elif cavityType == 0: 
            if failedCavType == 1: 
                name = 'SPK'
            elif failedCavType == 2:
                name = 'MBL'
            elif failedCavType == 3:
                name = 'HBL'
            else:
                name = 'Wrong cavity defined in list_z'
            print('\033[91m' + 'Failed_Cavity(' + name + '_' + str(cavNbrs[i]) + ') \t' + '\033[0m')
        else:
            print('something wrong in list_z (error 2)')
    
    ###########################################################
    #      perhaps add an estimation of time-consumtion
    ###########################################################
    
    return head, Lattice_Section_F, M_ref, Rematch_Cavities_index, margins


# In[56]:


def Lattice_2_Matrix(lattice, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL):
    """ A functions that uses the infromation in the lattice file to cerate a marix for
    teh accelerator-section defined by lattice.
    
    Input:
        - lattice: A matrix containing the information from the file Ref_Phase of the evaluated section
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: teh field map of a HBL cavity
        
    Output:
        - M: the transfer matrix of the acceletaror section defined by the lattice matrix.
            It will be the transfermatrix acting on (x, x', y, y', z, delta) where delta = dp/p_s
    
    """
    
    if (len(lattice[0,:]) != 10):
        print('Wrong input lattice in Lattice_2_Matrix')
    
    Input_Energy = lattice[0,5]
    
    M = np.identity(6)
    for i in range (0, len(lattice[:,0])):
        elementType = lattice[i,0]
        if elementType == 4: #drift
            Phase_Shift, Output_Energy, M_step = Drift_Matrix(Input_Energy, lattice[i,1]/1000) #convert to m
            M = M_step@M
            Input_Energy = Output_Energy
        elif elementType == 5: #quad
            Phase_Shift, Output_Energy, M_step = Quad_Matrix(Input_Energy, lattice[i, 1]/1000, lattice[i,2])
            M = M_step@M
            Input_Energy = Output_Energy
        elif elementType == 1 or elementType == 2 or elementType == 3: #cavity
            if elementType ==1:
                Field_Map = Field_Map_SPK
            elif elementType == 2:
                Field_Map = Field_Map_MBL
            else:
                Field_Map = Field_Map_HBL
            
            Phase_Shift, Output_Energy, M_step = Cavity_Matrix_Input_Phase(elementType, lattice[i, 3], Input_Energy, lattice[i, 2], Field_Map)
            M = M_step@M
            Input_Energy = Output_Energy
        elif elementType == 0: #failed cavity
            Phase_Shift, Output_Energy, M_step = Drift_Matrix(Input_Energy, lattice[i,1]/1000)
            M = M_step@M
            Input_Energy = Output_Energy
        else:
            print('Error in Lattice_2_Matrix')
            
            
    return M


# In[57]:


def Drift_Matrix(Input_Energy, Length):
    """ Calculates the tranfer matrix of a drift space with length Length.
        TransferMatrix acts on (x, x', y, y', z, delta), where delta = dp/p_s
    
    Input:
        - Input_Energy: the energy of the beam before the drift space (unit: MeV)
        - Length: the length of the drift space (unit: mm)
        
    Output:
        - Phase_Shift: of the beam after the driftspace (units: degrees)
        - Output_Energy: the energy of the beam after the drift space (unit: MeV)
        - M: the transfer matrix of the dirft space
        
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    f = cnst.f
    gamma = Input_Energy/m0+1
    beta = np.sqrt(1-1/gamma**2)
    L = Length
    
    Mx =[[1, L], [0, 1]]
    My = Mx
    Mz = [[1, L/gamma**2], [0, 1]]
    M0 = np.zeros((2, 2))
    
    #M = [[Mx, M0, M0], [M0, My, M0], [M0, M0, Mz]]
    M1 = np.concatenate((Mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, My, M0), axis = 1)
    M3 = np.concatenate((M0, M0, Mz), axis = 1)
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    Output_Energy = Input_Energy
    Phase_Shift = 360*f*L/(beta*c)
    
    return Phase_Shift, Output_Energy, M


# In[58]:


def Quad_Matrix(Input_Energy, Length, Gradient):
    """ Calculates the tranfer matrix of a quadropol of length Length and gradient Gradient.
        TransferMatrix acts on (x, x', y, y', z, delta), where delta = dp/p_s
    
    Input:
        - Input_Energy: the energy of the beam before the quadropol (unit: MeV)
        - Length: the length of the quadropol (unit: mm)
        - Gradient: The strength of magnetiocs fields gradient in the quadropol (unit: T/m)
        
    Output:
        - Phase_Shift: of the beam after the driftspace (units: degrees)
        - Output_Energy: the energy of the beam after the drift space (units: MeV)
        - M: the transfer matrix of the dirft space
    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    f = cnst.f
    gamma = Input_Energy/m0+1
    beta = np.sqrt(1-1/gamma**2)
    L = Length
    G = Gradient
    
    Bro = m0*1e+6/c*beta*gamma #from P/q = m0 gamma beta c /e = E0 10^6 beta/c if E0 is given in MeVs
    w = np.sqrt(abs(G/Bro)) #in case G is negative
    
    if G >= 0: #focusing
        Mx=[[np.cos(w*L), np.sin(w*L)/w],[-w*np.sin(w*L), np.cos(w*L)]]
        My=[[np.cosh(w*L), np.sinh(w*L)/w],[w*np.sinh(w*L), np.cosh(w*L)]]
    else: #defocusing
        My=[[np.cos(w*L), np.sin(w*L)/w],[-w*np.sin(w*L), np.cos(w*L)]]
        Mx=[[np.cosh(w*L), np.sinh(w*L)/w],[w*np.sinh(w*L), np.cosh(w*L)]]

    Mz=[[1, L/gamma**2],[0, 1]] #drift space in z-direction
    M0 = np.zeros((2, 2))

    M1 = np.concatenate((Mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, My, M0), axis = 1)
    M3 = np.concatenate((M0, M0, Mz), axis = 1)
        
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    Output_Energy = Input_Energy
    Phase_Shift = 360*f*L/(beta*c)
    
    return Phase_Shift, Output_Energy, M


# In[59]:


def Error_z(x, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL, Lattice_Section_F, M_ref, head, Rematch_Cavities_index):
    """ A fucntions that deterines the godness of the solution x. 
    The better the solution is the higher the value is. Highest value is zero
    
    Input:
        - x: a vector containing a solution consiting of the amplitudes for the four cavities followed 
                by the phase of each cavity for the cavities subjected to remaching.
        - Field_Map_SPK: the field map of a spoke cavity
        - Field_Map_MBL: the field map of a medium-beta cavity
        - Field_Map_HBL: the field map of a high-beta cavity
        - Lattice_Section_F: The four closest caveties to the failed one are chosen for rematch. This is the lattice 
                    that contains all elements between the two outermost caveties of the five chosen ones.
        - M_ref: the reference transfer matrix of the fully functioning accelerator. (Just the five chosen 
                    cavities) Should be a 6x6 matrix.
        - head: number of the first element of index_f in the section in the big lattice.
        - Rematch_Cavities_index: position of cavities to be rematched: column 1: type, column 2: absolute 
                                    position, column 3: relative position in current section
        
    Output:
        - fitness_Value = a value between 0 and inf that determines how good the solution x is.
        The higher number the better.
    
    """
    
    #c = cnst.c
    #m0 = cnst.m0
    
    #start by creating a lattice for the section using the settings defined by x
    reMat_Lattice_Section = Lattice_Section_F.copy()
    N = len(reMat_Lattice_Section[:,0]) #number of elements in the section of the lattice we are examining
    
    i = 0 #idx in Lattice_Section_F
    k = 0 #idx of cavity w/o the offline cavity
    
    while (i < N) and (k < 5):
        elementType = reMat_Lattice_Section[i,0]
        if elementType == 1 or elementType == 2 or elementType == 3: #i.e. it is a functioning cavity
            #change the amplitude
            reMat_Lattice_Section[i,2] = reMat_Lattice_Section[i,2]*x[k]
            #change the input_phase
            reMat_Lattice_Section[i,3] = reMat_Lattice_Section[i,3]+x[k+4]
            k = k+1
        i = i+1
    
    M_temp = Lattice_2_Matrix(reMat_Lattice_Section, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL)
    
    #err = max(max(abs(np.subtract(M_temp[4:, 4:], M_ref[4:,4:]))))
    #only looking at the longitudinal matching for now, will correct transversal matching w quads
    
    temp1 = np.subtract(M_temp[4:, 4:],M_ref[4:,4:])
    
    temp2 = abs(temp1)
     
    temp5 = np.amax(temp2)

    err = temp5
    
    return err


# In[60]:


def Ref_RePhase(Field_Map_SPK, Field_Map_MBL, Field_Map_HBL, head, Lattice_Section_F, Rematch_Cavities_index, opt_sol, Ref_Phase):
    """
    A function that changes the downstream phase after the cavity rematch. (does not need to take quad rematch into account)
    
    Input:
        - Field_Map_SPK: the field map of a spoke cavity
        - Field_Map_MBL: the field map of a medium-beta cavity
        - Field_Map_HBL: the field map of a high-beta cavity
        - head: number of the first element of Lattice_Section_F in the section in the big lattice.
        - Lattice_Section_F: The four closest caveties to the failed one are chosen for rematch. This is the lattice 
                    that contains all elements between the two outermost caveties of the five chosen ones.
        - Rematch_Cavities_index: position of cavities to be rematched: column 1: type, column 2: absolute 
                                    position, column 3: relative position in current section
        - opt_sol: a [1, nvars] vector containing the optimal amplitude and input ohases for the remached cavities.
        - Ref_Phase: the matrix containing all information in the previously created file ref_Phase.
                A matrix of 10 columns where each row corresponds to a compondent in the accelerator.
                The 10 columns are: (0)Type, (1)length (mm), (2)gradient(T/m) or AMP, (3)Input_Phase, (4)Sync_Phase, 
                (5)Input_Energy, (6)Output_Energy, (7)d_Phy, (8)Full_length(m), (9)Abs_Phase
                All these are expained in the function Ref_Phase
                In short, the lattice file of the fully functioning accelerator
        
    Output:
        - Ref_rephase: A matrix corresponding to the lattice of the rematched lattice. 
                Note that no quad-ramatch have been made yet, but cavity rematch and downstream phase change.
                A matrix of 10 columns where each row corresponds to a compondent in the accelerator.
                The 10 columns are: (0)Type, (1)length (mm), (2)gradient(T/m) or AMP, (3)Input_Phase, (4)Sync_Phase, 
                (5)Input_Energy, (6)Output_Energy, (7)d_Phy, (8)Full_length(m), (9)Abs_Phase
                All these are expained in the function Ref_Phase
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    temp_Lattice = Lattice_Section_F.copy()
    
    #change the amplitude and phase of the failed cavity
    k=0
    for i in range(len(Rematch_Cavities_index[:,0])):
        if Rematch_Cavities_index[i,0] != 0:
            #the relative index in the section of Lattice of the cavity to be set
            rel_Idx = Rematch_Cavities_index[i,2] 
            
            #set new amplitude
            AMP = temp_Lattice[rel_Idx,2]
            AMP = AMP*opt_sol[k]
            temp_Lattice[rel_Idx,2] = AMP
            
            #set new phase
            phy = temp_Lattice[rel_Idx, 3]
            phy = phy + opt_sol[k+4]
            temp_Lattice[rel_Idx, 3] = phy
            
            k +=1
            if(k>4):
                print('\n Too many cavities to be reset in Ref_RePhase \n')
    
    Ref_rephase = Ref_Phase.copy()
    #switch the section on the working accelerator for the rematched section of the failed accelertor in Ref_Phase
    Ref_rephase[head:head+len(temp_Lattice[:,0]),:] = temp_Lattice
    
    #need to change energy and phase for all elements after the first rematched cavity.
    
    #first elements in remaching section
    #input energy and abs phase for the first element
    Output_Energy = Ref_rephase[head-1,5]
    abs_Phase = Ref_rephase[head-1,9]
    z = Ref_rephase[head-1,8]
    for i in range(head, head + len(temp_Lattice[:,0])):
        elementType = Ref_rephase[i,0]
        Input_Energy = Output_Energy
        
        if elementType == 0: #failed cavity
            f = cnst.f #Hz
            gamma = 1 + Input_Energy/m0
            beta = np.sqrt(1-1/gamma**2)
            L = Ref_rephase[i,1]/1000 #to convert from mm to m
            d_Phy = 360*f*L/(beta*c)
            abs_Phase = abs_Phase + d_Phy
            #z will not change
            
            print("\n in Ref_rephase the AMP of the failed cavity should be zero: " + str(Ref_rephase[i,2]) + "\n")
            #already set in Ref_Phase is type, length, AMP, z should also not change
            #need to set Input_Energy, Output_Energy, d_Phy, abs_Phase, synch_Phase, input_Phase
            Ref_rephase[i,3] = 0 #setting input_Phase to 0
            Ref_rephase[i,4] = 0 #Setting Synch_Phase to 0
            Ref_rephase[i,5] = Input_Energy
            Ref_rephase[i,6] = Output_Energy
            Ref_rephase[i,7] = d_Phy
            Ref_rephase[i,9] = abs_Phase
            
        elif elementType == 4 or elementType == 5: #drift or quad
            f = cnst.f #Hz
            gamma = 1 + Input_Energy/m0
            beta = np.sqrt(1-1/gamma**2)
            L = Ref_rephase[i,1]/1000 #to convert from mm to m
            d_Phy = 360*f*L/(beta*c)
            abs_Phase = abs_Phase + d_Phy
            
            Ref_rephase[i,5] = Input_Energy
            Ref_rephase[i,6] = Output_Energy
            Ref_rephase[i,7] = d_Phy
            Ref_rephase[i,9] = abs_Phase
            
        elif elementType == 1 or elementType == 2 or elementType == 3:
            AMP = Ref_rephase[i,2]
            L = Ref_rephase[i,1]/1000
            Input_Phase = Ref_rephase[i,3]
            
            while Input_Phase < -180:
                Input_Phase += 360
            while Input_Phase > 180:
                Input_Phase -= 360
            
            if(elementType == 1):
                Field_Map = Field_Map_SPK
            elif(elementType == 2):
                Field_Map = Field_Map_MBL
            elif(elementType == 3):
                Field_Map = Field_Map_HBL
            d_Phy, Output_Energy, M = Cavity_Matrix_Input_Phase(elementType, Input_Phase, Input_Energy, AMP, Field_Map)
            abs_Phase += d_Phy
            Synch_Phase = Input_to_Sync(elementType, Input_Phase, Input_Energy, AMP, Field_Map)
            
            Ref_rephase[i,4] = Synch_Phase
            Ref_rephase[i,5] = Input_Energy
            Ref_rephase[i,6] = Output_Energy
            Ref_rephase[i,7] = d_Phy
            Ref_rephase[i,9] = abs_Phase
        
        else:
            print("something wrong in Ref_rephase, error 1")
            
    
    #then all the elements following the remacthing section
    for i in range(head+len(temp_Lattice[:,0]), len(Ref_rephase[:,0])):
        elementType = Ref_rephase[i,0]
        Input_Energy = Output_Energy
        
        if elementType == 0: #failed cavity
            f = cnst.f #Hz
            gamma = 1 + Input_Energy/m0
            beta = np.sqrt(1-1/gamma**2)
            L = Ref_rephase[i,1]/1000 #to convert from mm to m
            d_Phy = 360*f*L/(beta*c)
            abs_Phase = abs_Phase + d_Phy
            #z will not change
            
            #already set in Ref_Phase is type, length, AMP, Input_Phase, z shoudl also not change
            #need to set Input_Energy, Output_Energy, d_Phy, abs_Phase, synch_Phase
            Ref_rephase[i,4] = 0 #Setting Synch_Phase to 0
            Ref_rephase[i,5] = Input_Energy
            Ref_rephase[i,6] = Output_Energy
            Ref_rephase[i,7] = d_Phy
            Ref_rephase[i,9] = abs_Phase
            
        elif elementType == 4 or elementType == 5: #drift or quad
            f = cnst.f #Hz
            gamma = 1 + Input_Energy/m0
            beta = np.sqrt(1-1/gamma**2)
            L = Ref_rephase[i,1]/1000 #to convert from mm to m
            d_Phy = 360*f*L/(beta*c)
            abs_Phase = abs_Phase + d_Phy
            
            Ref_rephase[i,5] = Input_Energy
            Ref_rephase[i,6] = Output_Energy
            Ref_rephase[i,7] = d_Phy
            Ref_rephase[i,9] = abs_Phase
            
        elif elementType == 1 or elementType == 2 or elementType == 3:
            AMP = Ref_rephase[i,2]
            Synch_Phase = Ref_rephase[i,4]
            
            if(elementType == 1):
                Field_Map = Field_Map_SPK
            elif(elementType == 2):
                Field_Map = Field_Map_MBL
            elif(elementType == 3):
                Field_Map = Field_Map_HBL
            else:
                print("something wrong w Ref_rephase, error 2")
            
            #calculate the new input phase that will give the most energy gain for this new lower energy
            Input_Phase = 0
            peak = Phase_Scan_Peak(elementType, AMP, Input_Energy, Field_Map, -180, 30, 180)
            peak = Phase_Scan_Peak(elementType, AMP, Input_Energy, Field_Map, peak-30, 5, peak+30)
            Input_Phase = peak+Synch_Phase
            Input_Phase = Phase_Scan(elementType, AMP, Synch_Phase, Input_Energy, Field_Map, Input_Phase-5, 1, Input_Phase+5)
            Input_Phase = Phase_Scan(elementType, AMP, Synch_Phase, Input_Energy, Field_Map, Input_Phase-1, 0.2, Input_Phase+1)
            Input_Phase = Phase_Scan(elementType, AMP, Synch_Phase, Input_Energy, Field_Map, Input_Phase-0.2, 0.04, Input_Phase+0.2)
            
            while Input_Phase <-180:
                Input_Phase = Input_Phase + 360
            while Input_Phase > 180:
                Input_Phase = Input_Phase - 360
            
            L = Ref_rephase[i,1]/1000
            d_Phy, Output_Energy, M = Cavity_Matrix_Input_Phase(elementType, Input_Phase, Input_Energy, AMP, Field_Map)
            abs_Phase += d_Phy
            Synch_Phase = Input_to_Sync(elementType, Input_Phase, Input_Energy, AMP, Field_Map)
            
            Ref_rephase[i,3] = Input_Phase
            Ref_rephase[i,4] = Synch_Phase
            Ref_rephase[i,5] = Input_Energy
            Ref_rephase[i,6] = Output_Energy
            Ref_rephase[i,7] = d_Phy
            Ref_rephase[i,9] = abs_Phase
        else:
            print("something wrong in Ref_rephase, error 3")
        
    return Ref_rephase


# In[61]:


def Phase_Scan_Peak(elementType, AMP, Input_Energy, Field_Map, bg, step, ed):
    """
    Calculates the Input_phase where the energy gain is maximum.
    
    Input:
        - elementType: Type of element (1 = SPK, 2 = MBL or 3 = HBL)
        - AMP: how much of the power in used to generate the field in Field_Map the cavity is using
        - Input_Energy: the energy of the synchrinous particle enetring this cavity [MeV] GOTTA BE THE SYNCHRONOUS PARTICLE RIGHT?
        - Field_Map: the field_map of a cavito of type elementType. Must be an array. [z (m), Ex (MV/m), Ey (MV/m), Ez (MV/m)]
        - bg: the beginning of the scan in degrees
        - step: the stepsize of the scan
        - ed: the end of the scan in degrees
        
    Output:
        - peak: the Input_phase where the energy gain is maximum
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    if elementType == 1:
        f = cnst.f #Hz
    elif elementType == 2 or elementType == 3:
        f = 2*cnst.f #Hz
    else:
        print("Something is wrong in Phase_Scan_Peak")
    
    Ez = Field_Map[:,3]*AMP #MV/m
    z = Field_Map[:,0] #m
    
    N = len(Ez)
    L_total = z[N-1] #total length of the field map
    L=L_total/(N-1) #length per step
    
    gamma_i = Input_Energy/m0+1
    
    N_steps = np.floor((ed-bg)/step)+1
    peak = bg
    idx = 0
    E_gain_max = 0
    
    for j in np.arange(bg, ed, step):
        gamma_i = Input_Energy/m0+1 
        gamma_i_0 = gamma_i 
        In_Phase = j*2*np.pi/360
        phiss = np.zeros(N) #Input phase for each gap
        
        for i in range(N):
            phiss[i] = In_Phase
            
            gamma_o = gamma_i+Ez[i]*np.cos(In_Phase)*L/m0
            temp = 1-1/gamma_o**2
            if temp<0:
                print("error in Phase_Scan_Peak, should be positive: " + str(temp) + " where gamma_o is " + str(gamma_o))
            beta_o = np.sqrt(1-1/gamma_o**2)
            
            d_phy = 2*np.pi*f*L/(beta_o*c)
            In_Phase = In_Phase + d_phy
            
            gamma_i = gamma_o
        
        E_gain_new = (gamma_o-gamma_i_0)*m0
        if E_gain_new > E_gain_max:
            peak = j
            E_gain_max = E_gain_new
    
    return peak


# In[62]:


def Phase_Scan(elementType, AMP, Synch_Phase, Input_Energy, Field_Map, bg, step, ed):
    """
    Calculated the input phase of a cavity when the synchronous phase is known.
    
    Input:
        - elementType: Type of element (1 = SPK, 2 = MBL or 3 = HBL)
        - AMP: how much of the power in used to generate the field in Field_Map the cavity is using
        - Synch_Phase: The phase of the synchronous particle in the cavity
        - Input_Energy: the energy of the synchrinous particle enetring this cavity [MeV] GOTTA BE THE SYNCHRONOUS PARTICLE RIGHT?
        - Field_Map: the field_map of a cavito of type elementType. Must be an array. [z (m), Ex (MV/m), Ey (MV/m), Ez (MV/m)]
        - bg: the beginning of the scan in degrees
        - step: the stepsize of the scan
        - ed: the end of the scan in degrees
    
    Output:
        - phase1: The Input_Phase of a cavity when the synchronous phase is known
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    if elementType == 1:
        f = cnst.f #Hz
    elif elementType == 2 or elementType == 3:
        f = 2*cnst.f #Hz
    else:
        print("Something is wrong in Phase_Scan_Peak")
    
    Ez = Field_Map[:,3]*AMP #MV/m
    z = Field_Map[:,0] #m
    
    N = len(Ez)
    L_total = z[N-1] #total length of the field map
    L=L_total/(N-1) #length per step
    
    ref = 1.7976931348623157e+308 #the maximum float value in python
    phase1 = bg
    
    for j in np.arange(bg, ed, step):
            
        gamma_i = Input_Energy/m0+1
        gamma_i_0 = gamma_i
        
        In_Phase = j*2*np.pi/360;
        phiss = np.zeros(N)
        
        for i in range(N):
            gamma_o = gamma_i + Ez[i]*np.cos(In_Phase)*L/m0
            temp = 1-1/gamma_o**2
            if temp<0:
                print("error in Phase_Scan")
            beta_o = np.sqrt(1-1/gamma_o**2)
            
            d_phy = 2*np.pi*f*L/(beta_o*c)
            In_Phase =In_Phase+d_phy
            
            gamma_i = gamma_o
        
        a = trapz(z, np.multiply(Ez,np.sin(phiss)))
        b = trapz(z, np.multiply(Ez,np.cos(phiss)))
        ps = a/b
        tanfs = np.tan(Synch_Phase*2*np.pi/360)
        
        E_gain = (gamma_o-gamma_i)*m0
        temp = np.abs(ps-tanfs)
        
        if temp < ref:
            phase1 = j
            ref = np.abs(ps-tanfs)
    
    return phase1
    


# In[63]:


def List_t(Field_Map_SPK, Field_Map_MBL, Field_Map_HBL, head, Lattice_Section_F, Rematch_Cavities_index, Ref_rephase):
    """
    Find the positions of the Quadrupoles that are going to be rematched.
    
    Input:
        - Field_Map_SPK: the field map of a spoke cavity
        - Field_Map_MBL: the field map of a medium-beta cavity
        - Field_Map_HBL: the field map of a high-beta cavity
        - head: number of the first element of Lattice_Section_F in the section in the big lattice.
        - Lattice_Section_F: The four closest caveties to the failed one are chosen for rematch. This is the lattice 
                    that contains all elements between the two outermost caveties of the five chosen ones.
        - Rematch_Cavities_index: position of cavities to be rematched: column 1: type, column 2: absolute 
                                    position, column 3: relative position in current section
        - Ref_rephase: A matrix corresponding to the lattice of the rematched cavities. 
                Note that no quad-ramatch have been made yet, but cavity rematch and downstream phase change.
                A matrix of 10 columns where each row corresponds to a compondent in the accelerator.
                The 10 columns are: (0)Type, (1)length (mm), (2)gradient(T/m) or AMP, (3)Input_Phase, (4)Sync_Phase, 
                (5)Input_Energy, (6)Output_Energy, (7)d_Phy, (8)Full_length(m), (9)Abs_Phase
                All these are expained in the function Ref_Phase
    
    Output:
        - head_t: The starting index of Lattice_Section_Quads in the big Ref_rephase matrix
        - Lattice_Section_Quads: A part of the Ref_rephase matrix countaing all the quads that are being rematched
        - Mt_ref: The reference matrix for the lattice_section contaiing all the quads that are being rematched
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    former = 2
    later = 2
    row = np.where(Ref_rephase[:,0] == 0)[0][0]
    N_total = len(Ref_rephase[:,0])
    
    head_t = head
    tail_t = head + len(Lattice_Section_F[:,0])-1
    
    while head_t >= 0 and sum(Ref_rephase[head_t:row,0]==5) < former:
        head_t += -1
        
    while tail_t <= N_total and sum(Ref_rephase[row: tail_t,0]==5) < later:
        tail_t += 1
    
    Lattice_Section_Quads = Ref_rephase[head_t:tail_t,:]
    Mt_ref = Lattice_2_Matrix(Lattice_Section_Quads, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL)
    
    return head_t, Lattice_Section_Quads, Mt_ref


# In[64]:


def Matrix_calculation_after_z_before_t(Lattice_Section_Quads, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL):
    ##################################### never used #######################################################
    """
    Calculates the transfer matrices between the quads for the Section of the Lattice contaiing all 4 
    quadrupoles to be remacthed. The longitudinal mathcing should already have been done for the other elements.
    So the lattice section will consist of transfer matrices M1-Q1-M2-Q2-M3-Q3-M4-Q4-M5. Here only the M-matrices
    are calculated.
    
    Input:
        - Lattice_Section_Quads: A part of the Ref_rephase matrix countaing all the quads that are being rematched.
        - Field_Map_SPK: the field map of a spoke cavity
        - Field_Map_MBL: the field map of a medium-beta cavity
        - Field_Map_HBL: the field map of a high-beta cavity
    
    Output:
        - M1: The transfer matrix for all elements before the first Quadrupole in Lattice_Section_Quads.
        - E1: Input energy to the first Quandrupole in Lattice_Section_Quads.
        - M2: The transfer matrix of the elements between Quadrupole 1 and 2 in Lattice_Section_Quads.
        - E2: Input energy to the second Quandrupole in Lattice_Section_Quads.
        - M3: The transfer matrix of the elements between Quadrupole 2 and 3 in Lattice_Section_Quads.
        - E3: Input energy to the third Quandrupole in Lattice_Section_Quads.
        - M4: The transfer matrix of the elements between Quadrupole 3 and 4 in Lattice_Section_Quads.
        - E4: Input energy to the fourth Quandrupole in Lattice_Section_Quads
        - M2: The transfer matrix for all elements after the last Quadrupole in Lattice_Section_Quads
    """
    
    #find the position of all quadrupoles in Lattice_Section_Quads
    quad_Idxs = np.where(Lattice_Section_Quads[:,0] == 5)
    quad_Idxs = quad_Idxs[0]
    
    Input_Energy = Lattice_Section_Quads[0,5]
    
    if quad_Idxs[0] == 0:
        M1 = np.identity(6)
    else:
        #put together a matrix
        M1 = Lattice_2_Matrix(Lattice_Section_Quads[0:quad_Idxs[0]-1,:], Field_Map_SPK, Field_Map_MBL, Field_Map_HBL)
    #The Input Energy of the Quadrupole is saved in the Lattice_Section_Quads
    E1 = Lattice_Section_Quads[quad_Idxs[0],5]
    
    M2 = Lattice_2_Matrix(Lattice_Section_Quads[quad_Idxs[0]+1:quad_Idxs[1]-1,:], Field_Map_SPK, Field_Map_MBL, Field_Map_HBL)
    
    E2 = Lattice_Section_Quads[quad_Idxs[1],5]
    
    M3 = Lattice_2_Matrix(Lattice_Section_Quads[quad_Idxs[1]+1:quad_Idxs[2]-1,:], Field_Map_SPK, Field_Map_MBL, Field_Map_HBL)
    
    E3 = Lattice_Section_Quads[quad_Idxs[2],5]
    
    M4 = Lattice_2_Matrix(Lattice_Section_Quads[quad_Idxs[2]+1:quad_Idxs[3]-1,:], Field_Map_SPK, Field_Map_MBL, Field_Map_HBL)
    
    E4 = Lattice_Section_Quads[quad_Idxs[3],5]
    
    if quad_Idxs[3] == len(Lattice_Section_Quads)-1:
        M5 = np.identity(6)
    else:
        M5 = Lattice_2_Matrix(Lattice_Section_Quads[quad_Idxs[3]+1:-1,:], Field_Map_SPK, Field_Map_MBL, Field_Map_HBL)
    
    return M1, E1, M2, E2, M3, E3, M4, E4, M5


# In[65]:


def Error_t(x, Lattice_Section_Quads, M_ref, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL):
    """
    Calculates the difference between the fullly functioning Lattice section defined by Lattice_Section_Quads
    and the same section with the failed cavity and the rematched cavities and quadrupoles. Only the
    transverse part of the matrices are compared.
    
    Input:
        - x: a vector containing a solution consiting of the gradient settings of the four quadropoles 
                subjected to remaching.
        - Lattice_Section_Quads: A part of the Ref_rephase matrix countaing all the quads that are being rematched.
        - Mt_ref: The reference matrix for the lattice_section contaiing all the quads that are being rematched
        
    Output:
        - err: The biggest element in M_new-M_ref. The new calculated transfer matrix - the reference transfer martix.
    """
    
    temp_Lattice = Lattice_Section_Quads.copy()
    
    #Change settings of the quad gradients
    idxs = np.where(temp_Lattice[:,0] == 5)
    idxs = idxs[0]
    if (len(idxs) != 4):
        print("Something wrong in Error_t, wrong number of quadrupoles in Lattice_Section_Quads: " + str(len(idxs)))
        
    k = 0
    for i in idxs:
        temp_Lattice[i,2] = temp_Lattice[i,2]*x[k]
        k += 1
    
    M_new = Lattice_2_Matrix(temp_Lattice, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL)
    
    temp1 = np.subtract(M_new[0:4, 0:4],M_ref[0:4,0:4])
    
    temp2 = abs(temp1)
     
    temp5 = np.amax(temp2)

    err = temp5
    
    return err


# In[ ]:


def Ref_rephase_Quad(Ref_rephase, head_t, Lattice_Section_Quads, Opt_sol_q):
    """
    A function that set the new quad settings defined by Opt_sol_q into Ref_rephase.
    
    Input:
        -  Ref_rephase: A matrix corresponding to the lattice of the rematched cavities. 
                Note that no quad-ramatch have been made yet, but cavity rematch and downstream phase change.
                A matrix of 10 columns where each row corresponds to a compondent in the accelerator.
                The 10 columns are: (0)Type, (1)length (mm), (2)gradient(T/m) or AMP, (3)Input_Phase, (4)Sync_Phase, 
                (5)Input_Energy, (6)Output_Energy, (7)d_Phy, (8)Full_length(m), (9)Abs_Phase
                All these are expained in the function Ref_Phase
        - head_t: The starting index of Lattice_Section_Quads in the big Ref_rephase matrix.
        - Lattice_Section_Quads: A part of the Ref_rephase matrix countaing all the quads that are being rematched.
        - Opt_sol_q: the best solution for the quadrupole strength change. a [4,1] vector with the setting change
                    for each quadrupole as elements.
    
    Out_put:
        - Ref_rephase_quad: A matrix corresponding to the lattice of the rematched cavities, downstream phase and
                rematched quadrupoles. 
                Note that no quad-ramatch have been made yet, but cavity rematch and downstream phase change.
                A matrix of 10 columns where each row corresponds to a compondent in the accelerator.
                The 10 columns are: (0)Type, (1)length (mm), (2)gradient(T/m) or AMP, (3)Input_Phase, (4)Sync_Phase, 
                (5)Input_Energy, (6)Output_Energy, (7)d_Phy, (8)Full_length(m), (9)Abs_Phase
                All these are expained in the function Ref_Phase
    """
    idxs = np.where(Lattice_Section_Quads[:,0] == 5)
    idxs = idxs[0]
    if len(idxs) != 4:
        print("wrong number of quads in Lattice_Section_Quads in ref_rephase_Quad")
        
    idxs = head_t + np.array(idxs) #the indexes in Ref_rephase of the rematched quads
    
    Ref_rephase_quad = Ref_rephase.copy()
    
    k = 0
    for i in idxs:
        Ref_rephase_quad[i,2] = Ref_rephase_quad[i,2]*Opt_sol_q[k]
        k += 1
    
    return Ref_rephase_quad


# In[66]:


def lattice_update(Ref_phase, cavType, newLatticeName, oldLatticeName):
    """
    Creates a lattice file with the the data from Ref_phase and saves it as a file with the name fileName.
    
    Input:
        - Ref_Phase: the matrix containing all information in the previously created file ref_Phase.
                A matrix of 10 columns where each row corresponds to a compondent in the accelerator.
                The 10 columns are: (0)Type, (1)length (mm), (2)gradient(T/m) or AMP, (3)Input_Phase, (4)Sync_Phase, 
                (5)Input_Energy, (6)Output_Energy, (7)d_Phy, (8)Full_length(m), (9)Abs_Phase
                All these are expained in the function Ref_Phase
        - cavType: the type fp the failed cavity
        - newLatticeName: The name of the uppdated lattice file
        - oldLatticeName: The filename of the original lattice file (of the fully functioning accelerator)
    """
    
    try:
        oldLt = open(oldLatticeName, "r")
        newLt = open(newLatticeName, "w")
        
        #header
        newLt.write('; Remacthed Lattice file \n')
        
        oldLine = oldLt.readline()
        
        newLt.write(oldLine)
        
        lastLineBool = ('END' in oldLine) and (len(oldLine)==4)
        
        #To deal with error in lattice-file
        nbrLines = 0
        maxLines = len(Ref_phase[:,0])
        
        while(lastLineBool == False): 
            oldLine = oldLt.readline() #reads one line and then moves onto the next
            lastLineBool = ('END' in oldLine) and (len(oldLine)==4)
            if oldLine != "" :#check if line is empty
                
                if nbrLines == 748:
                    #DRIFT has 5 letters, remove the 6th character to remove the extra whitespace in lattice file
                    oldLine = oldLine[0:5] + oldLine[7:]
                params = oldLine.split(' ')
                
                if nbrLines < maxLines:
                    elementType = Ref_phase[nbrLines,0]
                
                #capital/lowercase letters matter
                if  len(oldLine) > 4 and ('QUAD' in oldLine) and oldLine[4] == ' ':
                    if elementType != 5:
                        print("Wrong type in 'lattice_update', error 1")
                    
                    newLt.write(f"QUAD {params[1]} {Ref_phase[nbrLines,2]} {params[3]} {params[4]} {params[5]} {params[6]} {params[7]} {params[8]} {params[9]} \n")
                    nbrLines += 1
                elif len(oldLine) > 5 and ('DRIFT' in oldLine) and oldLine[5] == ' ':
                    if elementType != 4:
                        print("Wrong type in 'lattice_update', error 2")
                    nbrLines += 1
                    newLt.write(oldLine)
                     
                elif len(oldLine) > 9 and ('FIELD_MAP' in oldLine) and oldLine[9] == ' ':
                    
                    if ('Spoke_W_coupler' in oldLine):
                        if elementType == 0 and cavType == 1:
                            newLt.write('ERROR_CAV_NCPL_DYN 1 0 0 0 0 0 -100 0 0 \n') #to turn off the cavity
                        elif elementType != 1:
                            print("Wrong type in 'lattice_update', error 3")
                        
                        newLt.write(f"FIELD_MAP {params[1]} {params[2]} {Ref_phase[nbrLines, 3]} {params[4]} {params[5]} {Ref_phase[nbrLines, 2]} {params[7]} {params[8]} Spoke_W_coupler \n")
                    
                        nbrLines += 1
                        
                    elif ('MB_W_coupler' in oldLine):
                        if elementType == 0 and cavType == 2:
                            newLt.write('ERROR_CAV_NCPL_DYN 1 0 0 0 0 0 -100 0 0 \n') #to turn off the cavity
                        elif elementType != 2:
                            print("Wrong type in 'lattice_update', error 4")
                        
                        newLt.write(f"FIELD_MAP {params[1]} {params[2]} {Ref_phase[nbrLines, 3]} {params[4]} {params[5]} {Ref_phase[nbrLines, 2]} {params[7]} {params[8]} MB_W_coupler \n")
                    
                        nbrLines += 1
                        
                    elif ('HB_W_coupler' in oldLine):
                        if elementType == 0 and cavType == 3:
                            newLt.write('ERROR_CAV_NCPL_DYN 1 0 0 0 0 0 -100 0 0 \n') #to turn off the cavity
                        elif elementType != 3:
                            print("Wrong type in 'lattice_update', error 5")
                            
                        newLt.write(f"FIELD_MAP {params[1]} {params[2]} {Ref_phase[nbrLines, 3]} {params[4]} {params[5]} {Ref_phase[nbrLines, 2]} {params[7]} {params[8]} HB_W_coupler \n")
                    
                        nbrLines += 1
                else:
                    newLt.write(oldLine) #if it is not a Drift, Quad or Cavity it will remain the same
            else:
                newLt.write(oldLine) #keep an empty line empty
        
        newLt.write('END \n')
    
    finally:
        oldLt.close()
        newLt.close()

def readLattice2(latticeName):
    '''read lattice file and turn to a matrix with four columns. Cav read a failed cavity as failed
     
    Input:
         - lattice file with all components of the accelerator as lines with the parameters
             specified in the format used in TraceWin
    
    Output:
         - the name of a simplified lattice file with only five types of components.
             This file contains four columns specifed as:
             - Drift: 4, length, 0, 0
             - Quad:  5, length, strength/gradient, 0
             - SPK:   1, length, AMP, Input_Phase
             - MBL:   2, length, AMP, Input_Phase
             - HBL:   3, length, AMP, Input_Phase
             
             (All parameters extracted from the original lattice file)
    
    
    '''
    
    simpleLattice = 'simpleLattice.txt'
    
    #clear file if it already exists
    simLt = open('simpleLattice.txt', 'w').close()
    
    try:
        lt = open(latticeName, "r")
        simLt = open(simpleLattice, "w")
        
        #create header
        simLt.write('; drift: 4, length, 0, 0 \n; quad: 5, length, strength, 0 \n; SPK, MBL or HBL: (1,2 or 3), length, AMP, Input_Phase \n')
        
        tempLine = lt.readline()
        lastLineBool = ('END' in tempLine) and (len(tempLine) ==4)
        isCavOff = False
        
        #To deal with error in lattice-file
        nbrLines = 0
        #temp = 0
        
        while(lastLineBool == False): 
            tempLine = lt.readline() #reads one line and then moves onto the next
            if((';' in tempLine)== False):
                lastLineBool = ('END' in tempLine) and (len(tempLine) ==4)
            #temp += 1
            #print(f"lastlineBool = {lastLineBool} and length of line is {len(tempLine)} at line {temp} line is: {tempLine}")
            if tempLine != "" :#check if line is empty
                
                if ('ERROR_CAV_NCPL_DYN 1 0 0 0 0 0 -100 0 0' in tempLine):
                    isCavOff = True
                    print("does this happen then?")
                
                #if nbrLines == 748:
                #    #DRIFT has 5 letters, remove the 6th character to remove the extra whitespace in lattice file
                #    tempLine = tempLine[0:5] + tempLine[7:]
                params = tempLine.split(' ')
                
                #capital/lowercase letters matter
                if  len(tempLine) > 4 and ('QUAD' in tempLine) and tempLine[4] == ' ':
                    simLt.write(str(5) + ' ' + params[1] + ' ' + params[2] + ' 0 \n')
                    nbrLines += 1
                elif len(tempLine) > 5 and ('DRIFT' in tempLine) and tempLine[5] == ' ':
                    nbrLines += 1
                    simLt.write(str(4) + ' ' + params[1] + ' 0 0 \n')
                    
                elif len(tempLine) > 9 and ('FIELD_MAP' in tempLine) and tempLine[9] == ' ':
                    params = tempLine.split(' ')
                    if isCavOff == True:
                        simLt.write(f"4 {params[1]} 0 0 \n") #replacing off cavity with drift
                        isCavOff = False
                        print("does this happen?")
                    
                    elif ('Spoke_W_coupler' in tempLine):
                        #the order of the parameters are defined in traceWin
                        simLt.write(str(1) + ' ' + params[2] + ' ' + params[6] + ' ' + params[3] + ' \n')
                        nbrLines += 1
                        
                    elif ('MB_W_coupler' in tempLine):
                        simLt.write(str(2) + ' ' + params[2] + ' ' + params[6] + ' ' + params[3] + ' \n')
                        nbrLines += 1
                        
                    elif ('HB_W_coupler' in tempLine):
                        simLt.write(str(3) + ' ' + params[2] + ' ' + params[6] + ' ' + params[3] + ' \n')
                        nbrLines += 1        
    
    finally:
        lt.close()
        simLt.close()
        
    return simpleLattice

#rematch functions that take beam phase advance into account

def error_PABz(x, Ref_Phase, lengths, Rematch_Cavities_index, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, twiss_i):
    """
    A function that calculates a value on how good the longitudinal beam phase advace is. It should be decreasing and non oscillating. 
    
    Input:
        - x: a vector containing a solution consiting of the amplitudes for the four cavities followed 
                by the phase of each cavity for the cavities subjected to remaching.
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - length: a vector containg how many points of beta each element has [SPK, MBL, HBL, drift, quad]
        - Rematch_Cavities_index: position of cavities to be rematched: column 1: type, column 2: absolute 
                                    position, column 3: relative position in current section
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: the field map of a HBL cavity
        - twiss_i: 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z.
                
                
    Output:
        - err: a value between 0 and inf. The closer to zero the less oscillations.
    """
    
    #create a Ref_Phase matrix w the new settings
    if (len(Rematch_Cavities_index[:,0]) == 5) == False:
        print("in error_PASBz, wrong input of Rematch_Cavities_index")
    
    newRef_Phase = Ref_Phase.copy()
    
    k = 0
    for i in range(5):
        elementType = Rematch_Cavities_index[i,0]
        if elementType == 1 or elementType == 2 or elementType == 3: #i.e. it is a functioning cavity
            #change the amplitude
            idx = Rematch_Cavities_index[i,1]
            newRef_Phase[idx,2] = newRef_Phase[idx,2]*x[k]
            #change the input_phase
            newRef_Phase[idx,3] = newRef_Phase[idx,3]+x[k+4]
            k = k+1
    
    twiss_iz = twiss_i[:,2]
    betasz = twissFunc.twiss_beta_from_lattice_E1(newRef_Phase, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, twiss_iz, 3)
    PAz = twissFunc.phase_advance_from_beta1(betasz, newRef_Phase, lengths, 3)

    coeffs, residuals, rank, singular_values, rcond = np.polyfit(PAz[:,0], PAz[:,1], 1, full = True)
    #make a linear regression and negative slope is good and the least deviation from it?
    
    k = coeffs[0] #the average slope of the phase advance
    err = 0
    if k > 0:
        err = 9999999999
    else:
        err = abs(residuals)/1000 #to get size approx as big as the other error.
    return err

def error_PABxy(x, Ref_Phase, lengths, Qidxs, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, twiss_i):
    """
    A function that calculates a value on how good the longitudinal beam phase advace is. It should be decreasing and non oscillating. 
    
    Input:
        - x: a vector containing a solution consisting of the gradient settings of the four quadropoles 
                subjected to remaching.
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - length: a vector containg how many points of beta each element has [SPK, MBL, HBL, drift, quad]
        - Qidxs: position of quadrupoles to be rematched in Ref_Phase
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: the field map of a HBL cavity
        - twiss_i: 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z.
                
                
    Output:
        - err: a value between 0 and inf. The closer to zero the less oscillations.
    """
    
    #create a Ref_Phase matrix w the new settings
    
    newRef_Phase = Ref_Phase.copy()
    
    #need to get correct indexes of quadrupoles to do this
    k = 0
    for i in Qidxs:
        newRef_Phase[i,2] = newRef_Phase[i,2]*x[k]
        k += 1
    
    twiss_ixy = twiss_i[:,0:2]
    betasxy = twissFunc.twiss_beta_from_lattice_E1(newRef_Phase, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, twiss_ixy,12)
    PAxy = twissFunc.phase_advance_from_beta1(betasxy, newRef_Phase, lengths, 12)
    
    coeffsx, residualsx, rank, singular_values, rcond = np.polyfit(PAxy[:,0], PAxy[:,1], 1, full = True)
    coeffsy, residualsy, rank, singular_values, rcond = np.polyfit(PAxy[:,0], PAxy[:,2], 1, full = True)
    
    kx = coeffsx[0] #the average slope of the phase advance
    ky = coeffsy[0]
    err = 0
    if kx > 0 or ky > 0:
        err = 9999999999
    else:
        err = abs(residualsx+residualsy)/1000 #to get size approx as big as the other error.
        
        
        
    return err

#i want to check if residuals is what I think it is


