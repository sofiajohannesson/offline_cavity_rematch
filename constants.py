#!/usr/bin/env python
# coding: utf-8

# # natural constants

# In[ ]:


c = 2.99792458e8 #m/s, speed of light
m0 = 938.272029 #MeV, proton mass
f = 352.21e+6

