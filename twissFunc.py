#all functions used to analyse the twiss parameters of the lattice
#The other file got too big.

#adding twiss tp calculate phase_advance
import numpy as np
import constants as cnst
import rematch_functions as re_func

def Ref_Phase_twiss(simpleLatticeName, Input_Energy_0, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, twiss_i, typeBeta):
#no longer used
    """ Calculate the ref-phase for all elements in the simplified latttice of the accelerator
    
    Input:
        - simplifiedLatticeName: a file with four colums as. Type, length, parameter, parameter.
        - Input_Energy_0: The energy of the beam entering the lattice in MeV
        - FIELD_MAP_SPK: the field map of a SPK cavity
        - FIELD_MAP_MBL: the field map of a MBL cavity
        - FIELD_MAP_HBL: the field map of a HBL cavity
        - twiss_i: 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z.
        - typeBeta: either 0 for structure or 1 for beam calculation of beta
        
    Output:
        - PhaseFileName: The name of a txt-file containing data about all accelteretor components in 10 columns:
                            Type
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
         - Phase_adv: A Nx3 matrix containg thephaseadvace in units of degrees per m for all element in the lattice for
                     all three dimensions.
         - betas: A (N+1)x3 matrix containding the inital twiss beta folowwed by the beta values after each element in the lattice.
                 The three column is for x,y and z. N is the number of elements in the lattice defined by simplifiedLatticeName.
            
            If a parameter is not relevant for a component it will be given the value NaN
    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    #converting the textfile to a matrix (if the txt file already is created, only this needs to be done)
    
    latticeMatrix = np.loadtxt('simpleLattice.txt', delimiter = ' ', usecols = (0,1,2,3), comments = ';')
    
    Input_Energy = Input_Energy_0
    N = len(latticeMatrix[:,0])#number of element in latticeMatrix
    phaseFileName = 'Ref_Phase.txt'
    
    Phase_adv = np.zeros((N,3))
    
    if typeBeta == 6:
        betas = np.zeros((N+1,4))
    #elif typeBeta == 1:
        #betas = []
    elif typeBeta != 7:
        print(f"Wrong input of typeBeta in Ref_Phase_twiss, should be either 6 or 7 and is now: {typeBeta}")
    
    #save a list of paramenetrs for all elements
    #type, length(m), gradient(T/m), Input_Phase, Sync_Phase, Input_energy, Output_energy, d_Phy, Full_Length(m), abs_Phase
    #if an element does not have a value for one (or more) of these parameters, the value is assigned 'NaN'
    
    with open(phaseFileName, 'w') as phaseFile:
        for i in range(0,N):
            if i == 0:
                #write a header in the file before writing any parameters
                abs_Phase = 0
                Output_Energy = Input_Energy
                z = 0
                phaseFile.write('# Type, Length(mm), gradient(T/m) or AMP, Input_Phase(degree), Sync_Phase(degree), Input_energy(MeV), Output_energy(MeV), d_Phy(degree), Full_Length(m), abs_Phase(degree) \n')
                
                if typeBeta == 6:
                    betas[i,:] = [0, twiss_i[0,0], twiss_i[0,1], twiss_i[0,2]]
                elif typeBeta == 7:
                    #betas_firstRow  = [0, twiss_i[0,0], twiss_i[0,1], twiss_i[0,2]]
                    #betas.append(betas, betas_firstRow, axis=0)
                    betas = np.array([[0, twiss_i[0,0], twiss_i[0,1], twiss_i[0,2]]])
                
            else:
                Input_Energy = Output_Energy
                #ta också hänsyn till typeBeta här
                twiss_i = twiss_o
                
            typeElement = latticeMatrix[i,0]
            
            if typeElement == 4: # drift
                f = cnst.f #Hz ???????????????????????????????? is this true for all drifts, even the ones after the superconducting part?
                L = latticeMatrix[i,1]/1000 #in units of m
                z_i = z
                z = z+L
                #d_Phy = 360*f*L/(beta*c)
                d_Phy, Output_Energy,M = Drift_Matrix(Input_Energy,L)
                abs_Phase = abs_Phase + d_Phy
                phaseFile.write(f"{typeElement} {L*1000} NaN NaN NaN {Input_Energy} {Output_Energy} {d_Phy} {z} {abs_Phase} \n")
                
                twiss_o = twissTrM(twiss_i, M)
                if typeBeta == 6 :
                    
                    betas[i+1,0] = z
                    betas[i+1,1:]=twiss_o[0,:]
                    
                    gamma_i = 1 + Input_Energy/m0
                    beta_i = np.sqrt(1-1/gamma_i**2)
                    betagamma_i = beta_i*gamma_i
                    gamma_o = 1 + Output_Energy/m0
                    beta_o = np.sqrt(1-1/gamma_o**2)
                    betagamma_o = beta_o*gamma_o
                    
                    Phase_adv[i,:] = phase_adv_from_M(M, twiss_i[0,:], twiss_o[0,:], betagamma_i, betagamma_o,L)
                elif typeBeta == 7:
                    betas_element, twiss_o = DRIFT_dl(L, 100, twiss_i, z_i)
                    #print(f"elementType = {typeElement}, last row of beta_element: {betas_element[-1,:]}")
                    betas = np.append(betas, betas_element, axis=0)      
            
            elif typeElement == 5: #quad
                f = cnst.f # Hz
                #Input_Energy = Output_Energy
                #gamma = 1 + Input_Energy/m0
                #beta = np.sqrt(1-1/(gamma**2))
                L = latticeMatrix[i,1]/1000 # to units of m
                z_i = z
                z = z+L
                #d_Phy = 360*f*L/(beta*c)
                G = latticeMatrix[i,2]
                d_Phy, Output_Energy, M = Quad_Matrix(Input_Energy,L,G)
                abs_Phase = abs_Phase + d_Phy
                phaseFile.write(f"{typeElement} {L*1000} {G} NaN NaN {Input_Energy} {Output_Energy} {d_Phy} {z} {abs_Phase} \n")
                
                
                twiss_o = twissTrM(twiss_i, M)
                if typeBeta == 6:
                    #twiss_o = twissTrM(twiss_i, M)
                    betas[i+1,0] = z
                    betas[i+1,1:]=twiss_o[0,:]
                    
                    gamma_i = 1 + Input_Energy/m0
                    beta_i = np.sqrt(1-1/gamma_i**2)
                    betagamma_i = beta_i*gamma_i
                    gamma_o = 1 + Output_Energy/m0
                    beta_o = np.sqrt(1-1/gamma_o**2)
                    betagamma_o = beta_o*gamma_o
                    
                    Phase_adv[i,:] = phase_adv_from_M(M, twiss_i[0,:], twiss_o[0,:], betagamma_i, betagamma_o,L)
                elif typeBeta == 7:
                    gamma = 1 + Input_Energy/m0
                    beta = np.sqrt(1-1/gamma**2)
                    Bro = m0*1e+6/c*beta*gamma #magnetic rigidity from P/q = m0 gamma beta c /e = E0 10^6 beta/c if E0 is given in MeVs
                    K = G/Bro
                    betas_element, twiss_o = QUADK_dl(L, K, 100, twiss_i, z_i)
                    #print(f"elementType = {typeElement}, last row of beta_element: {betas_element[-1,:]}")
                    betas = np.append(betas, betas_element, axis=0)
                
            elif typeElement == 1 or typeElement == 2 or typeElement == 3:
                AMP = latticeMatrix[i,2]
                Input_Phase = latticeMatrix[i,3]
                #Input_Energy = Output_Energy #the energy the particle has coming out from the previous element
                L = latticeMatrix[i,1]/1000 #convert to m
                z_i = z
                z = z+L
                
                if typeElement ==1:
                    Field_Map = FIELD_MAP_SPK
                elif typeElement ==2:
                    Field_Map = FIELD_MAP_MBL
                else:
                    Field_Map = FIELD_MAP_HBL
                
                #using the matrix of a accelerating cavity
                Phase_Shift, Output_Energy, M, betas_element, twiss_o = Cavity_Matrix_Input_Phase_twiss(typeElement, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i, z_i) 
                
                d_Phy = Phase_Shift
                abs_Phase = abs_Phase + d_Phy
                Sync_Phase = Input_to_Sync(typeElement, Input_Phase, Input_Energy, AMP, Field_Map)
                
                phaseFile.write(f"{typeElement} {L*1000} {latticeMatrix[i,2]} {Input_Phase} {Sync_Phase} {Input_Energy} {Output_Energy} {d_Phy} {z} {abs_Phase} \n")
                
                #twiss_o = twissTrM(twiss_i, M)
                if typeBeta == 6:
                    twiss_o = twissTrM(twiss_i, M)
                    betas[i+1,0] = z
                    betas[i+1,1:]=twiss_o[0,:]
                    
                    gamma_i = 1 + Input_Energy/m0
                    beta_i = np.sqrt(1-1/gamma_i**2)
                    betagamma_i = beta_i*gamma_i
                    gamma_o = 1 + Output_Energy/m0
                    beta_o = np.sqrt(1-1/gamma_o**2)
                    betagamma_o = beta_o*gamma_o
                    
                    Phase_adv[i,:] = phase_adv_from_M(M, twiss_i[0,:], twiss_o[0,:], betagamma_i, betagamma_o,L)
                elif typeBeta == 7:
                    #print(f"elementType = {typeElement}, last row of beta_element: {betas_element[-1,:]}")
                    betas = np.append(betas, betas_element, axis=0)
                
    phaseFile.closed
    
    return phaseFileName, Phase_adv, betas

def twissTrM(twiss_i, M):
    """
    A function that preforms the transfromation from twiss_i to twiss_f casued by the element with
    the 6D transfer matrix M.
    
    Input:
        - twiss_i: a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values before entering the section defined by M.
        - M: a 6D transfer matrix of a lattice section or an element.
    
    Output:
        - twiss_o: a 3x3 matrix containing the twiss parameters efter passing through the section defined by M.
                The same arrangement of the parameters as twiss_i
    """
    twiss_o = np.zeros((3,3))
    
    for i in range(3):
        Mpart = M[i*2:i*2+2, i*2:i*2+2]
        #MInv = np.linalg.inv(Mpart) Not nessesary, looked at the difinition agan
        #t22 = MInv[0,0]
        #t12 = -MInv[0,1]
        #t21 = -MInv[1,0]
        #t11 = MInv[1,1]
        
        t11 = Mpart[0,0]
        t12 = Mpart[0,1]
        t21 = Mpart[1,0]
        t22 = Mpart[1,1]
        
        MTwiss = [[t11**2, -2*t11*t12, t12**2], [-t11*t21, t11*t22+t12*t21, -t12*t22], [t21**2, -2*t21*t22, t22**2]]
        #print("MTwiss")
        #print(MTwiss)
        
        Twiss_i = twiss_i[:,i]
        Twiss_i = np.reshape(Twiss_i, (3,1))
        #print("Twiss_i: " + str(Twiss_i))
        Twiss_f = MTwiss@Twiss_i
        #print("Twiss_f: " + str(Twiss_f))
        
        #if Twiss_f[0] < 0:
        #    print("error in TwissTrM, negative beta for dimension " + str(i))
            
        for j in range(3):
            twiss_o[j,i] = Twiss_f[j] #ugly solution but it did not want to cooperate

        # beta/gamma must always be above 0:
        for i in [0,2]: 
            if(twiss_o[i] < 0).any():
                raise ValueError(f"Calculated wrong Twiss matrix in column {i}: {twiss_o}")

    return twiss_o

def twissTrM_wFactor(twiss_i, M):
    """
    A function that preforms the transfromation from twiss_i to twiss_f casued by the element with
    the 6D transfer matrix M.
    
    Input:
        - twiss_i: a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values before entering the section defined by M.
        - M: a 6D transfer matrix of a lattice section or an element.
    
    Output:
        - twiss_o: a 3x3 matrix containing the twiss parameters efter passing through the section defined by M.
                The same arrangement of the parameters as twiss_i
    """
    twiss_o = np.zeros((3,3))
    
    for i in range(3):
        Mpart = M[i*2:i*2+2, i*2:i*2+2]
        #MInv = np.linalg.inv(Mpart) Not nessesary, looked at the difinition agan
        #t22 = MInv[0,0]
        #t12 = -MInv[0,1]
        #t21 = -MInv[1,0]
        #t11 = MInv[1,1]
        
        betagamma_o = 0
        betagamma_i = 0
        factor = np.sqrt(betagamma_o/betagamma_i)
        
        if i ==2: #for longitudinal
            factor = factor * (1/gamma_o^3) #^3?
        t11 = factor*Mpart[0,0]
        t12 = factor*Mpart[0,1]
        t21 = factor*Mpart[1,0]
        t22 = factor*Mpart[1,1]
        
        MTwiss = [[t11**2, -2*t11*t12, t12**2], [-t11*t21, t11*t22+t12*t21, -t12*t22], [t21**2, -2*t21*t22, t22**2]]
        #print("MTwiss")
        #print(MTwiss)
        
        Twiss_i = twiss_i[:,i]
        Twiss_i = np.reshape(Twiss_i, (3,1))
        #print("Twiss_i: " + str(Twiss_i))
        Twiss_f = MTwiss@Twiss_i
        #print("Twiss_f: " + str(Twiss_f))
        
        #if Twiss_f[0] < 0:
        #    print("error in TwissTrM, negative beta for dimension " + str(i))
            
        for j in range(3):
            twiss_o[j,i] = Twiss_f[j] #ugly solution but it did not want to cooperate

        # beta/gamma must always be above 0:
        for i in [0,2]: 
            if(twiss_o[i] < 0).any():
                raise ValueError(f"Calculated wrong Twiss matrix in column {i}: {twiss_o}")

    return twiss_o

def phase_adv_from_M(M, twiss_beta_i, twiss_beta_o, betagamma_i, betagamma_o, L):
    """
    A function that calculates the phase advance in x, y and z using the transfer matrix.
    If no energy is gained in the lattiec element the m21 element of the matrix is:
    m21 = sqrt(twiss_beta1*twiss_beta2)*sin(phase_adv)
    but if energy is gained the entire matrix needs to be mutliplied with sqrt(beta_i*gamma_i/(beta_o*gamma_o))
    
    Input:
        - M: A 6D transfer matrix
        - twiss_beta_i: a vector of length 3 where each value is twiss beta_i for x,y and z phase space.
                        The values of twiss beta before entering the element defined by M.
        - twiss_o: a vector of length 3 where each value is twiss beta_o for x,y and z phase space.
                        The values of twiss beta after exiting the element defined by M.
        - betagamma_i: the product of the relaticistic quantities beta and gamma before entering the element defined by M
        - betagamma_o: the product of the relaticistic quantities beta and gamma after exiting the element defined by M
        - L: the length (m) of the element defined by M
        
     Output:
         - Phase_Adv: a vector of length 3 containg the phase advance in x, y and z for the element defined by M.
                         The unit will be degrees/m
    
    """
    
        
    Phase_Adv = np.zeros(3)
    #print(f"L = {L} m")
    for i in range(3):
        t12 = M[i*2,i*2+1]
        temp = np.arcsin(np.sqrt(betagamma_o/betagamma_i)*t12/np.sqrt(twiss_beta_i[i]*twiss_beta_o[i]))
        #temp = np.arcsin(1/np.sqrt(twiss_beta_i[i]*twiss_beta_o[i]))
        Phase_Adv[i] = temp*360/(2*np.pi*L) #it gets wierd by dividing my the length of an element
        #Phase_Adv[i] = temp/L
        #Phase_Adv[i] = temp*360/(2*np.pi)
        
    
    return Phase_Adv

####################################################################################################
#                             functions inspired of the code of M. Eshraqi
####################################################################################################

def DRIFT_dl(L, n, twiss_i, z_i, gamma):
    '''
    Calculates the beta values for n equidistant points in a drift space.
    
    Input:
        - L: float, Length of the drift 
        - n: int, number of steps in the drift 
        - twiss_i: a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values before entering the drift space.
        - z_i: The position of the entrance of the drift space.
        - gamma: The relativistic gamma of the particles enetering the drift space.
    
    | 1    L |
    |        |
    | 0    1 | 
    
    Ouput:
        - betas_element: a 4xn matrix where each row is z, beta_x, beta_y and beta_z for n equidistant points in a drift space of length L.
        - twiss_o = a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values after exiting the drift space.
    
    '''
    
    #ele_type = 0
    dl = L/n
    
    betas_element = np.zeros((n,4))
    
    mx = np.array([[1, dl],[0, 1]])
    my = np.array([[1, dl],[0, 1]])
    mz = np.array([[1, dl/gamma**2],[0, 1]])
    M0 = np.array([[0,0],[0,0]])
    M1 = np.concatenate((mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, my, M0), axis = 1)
    M3 = np.concatenate((M0, M0, mz), axis = 1)
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    for i in range(n):
        twiss_o = twissTrM_E(twiss_i, M, gamma, gamma) 
        betas_element[i,1:] = twiss_o[0,:]
        betas_element[i,0] = z_i + (i+1)*dl
        twiss_i = twiss_o

    return betas_element, twiss_o

def QUADK_dl(L, K, n, twiss_i, z_i, gamma):
    '''
    Calculates the twiss beta values for n equidistand points in a thick quadrupole using the normalized gradient
    
    Input: 
        - L: float, Length of the quadrupole 
        - K: float, normalized gradient
        - n: int, number of steps in the quadrupole
        - twiss_i: a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values before entering the drift space.
        - z_i: The position of the entrance of the drift space.
        - gamma: The relativistic gamma of the particles enetering the drift space.

    | cos(dl*K**0.5)             sin(dl*K**0.5)/K**0.5 |
    |                                                  |
    | -sin(dl*K**0.5)*K**0.5     cos(dl*K**0.5)        |
    

    | cosh(dl*K**0.5)           sinh(dl*K**0.5)/K**0.5 |
    |                                                  |
    | sinh(dl*K**0.5)*K**0.5    cosh(dl*K**0.5)        |    
    
    Output:
        - betas_element: a 4xn matrix where each row is z, beta_x, beta_y and beta_z for n equidistant points in a thick quadrupole of length L with the normalized field strength K.
        - twiss_o = a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values after exiting the quadrupole.

    '''

    mx=np.eye(2)
    my=np.eye(2)
    mz=np.eye(2)

    ele_type = 1
    ele_type *= np.sign(K)
    dl = L/n
    
    betas_element = np.zeros((n,4))
    
    mz[0,1] = dl/gamma**2

    if K > 0:
        mx[0,0] = np.cos(dl*K**0.5)
        mx[1,1] = mx[0,0]
        mx[0,1] = np.sin(dl*K**0.5)/K**0.5
        mx[1,0] = -K*mx[0,1]
        
        my[0,0] = np.cosh(dl*K**0.5)
        my[1,1] = my[0,0]
        my[0,1] = np.sinh(dl*K**0.5)/K**0.5
        my[1,0] = K*my[0,1]
        
    elif K < 0:
        K*=-1
        my[0,0] = np.cos(dl*K**0.5)
        my[1,1] = my[0,0]
        my[0,1] = np.sin(dl*K**0.5)/K**0.5
        my[1,0] = -K*my[0,1]

        mx[0,0] = np.cosh(dl*K**0.5)
        mx[1,1] = mx[0,0]
        mx[0,1] = np.sinh(dl*K**0.5)/K**0.5
        mx[1,0] = K*mx[0,1]
        K*=-1
    elif K == 0:
        mx[0,1] = my[0,1] = dl

    M0 = np.array([[0,0],[0,0]])
    M1 = np.concatenate((mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, my, M0), axis = 1)
    M3 = np.concatenate((M0, M0, mz), axis = 1)
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    for i in range(n):
        twiss_o = twissTrM_E(twiss_i, M, gamma, gamma)
        betas_element[i,1:] = twiss_o[0,:]
        betas_element[i,0] = z_i + (i+1)*dl
        twiss_i = twiss_o
  
    return betas_element, twiss_o

####################################################################################################
#                          end of code ispired by M. Eshraqi
####################################################################################################


def Cavity_Matrix_Input_Phase_twiss(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i_0, z_i):
    """ 
    A function that decribes the particles movement in an accelerating cavity, with the twiss parameters included.
    
    Input:
         - elementType: 1 = spoke, 2 = MBL and 3 = HBL
         - Input_Phase: defined by the lattice file (WHAT DOES THIS MEAN? Not defined by the abs_phase before?) [degrees]
         - Input_Energy: the energy of the particle enetring this cavity [MeV]
         - AMP: how much of the maximum power the cavity is running at, set in the lattice file.
         - Field_Map: the field_map of the relevant cavity running at the maximum power. Must be an array. [z (m), Ex (MV/m), Ey (MV/m), Ez (MV/m)]
    
    Output:
         - Phase_Shift: The phase change of the beam as a result from travveling in the cavity [degrees]
         - Output_Energy: The energy of the beam a result of travelling in the cavity [MeV]
         - M: The matrix that descibes the cavities affect on the 6D phase-space of the beam [radian-m phase-space]
         - twiss_o = a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values after exiting the cavity.
    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    if elementType == 1:
        f = cnst.f #Hz
    elif elementType == 2 or elementType == 3:
        f = 2*cnst.f
    else:
        print(f"Not a cavity as elementType input to Cavity_Matrix_Input_Phase_twiss: {elementType}")
    
    Ez = Field_Map[:,3] # MV/m
    Ez = Ez*AMP
    z = Field_Map[:, 0] #m
    N = len(Ez)
    L_total = z[N-1]
    L = L_total/(N-1)
    
    betas_element = np.zeros((N,4))
    
    In_Phase = Input_Phase*2*np.pi/360 #convert phase to radians
    gamma_i = 1 + Input_Energy/m0 #m0 defined in 'variables'
    beta_i = np.sqrt(1-1/gamma_i**2)
    
    Mz = np.identity(2)
    Mx = np.identity(2)
    
    Phase_Shift = 0
    
    twiss_o = twiss_i_0
    
    for i in range(0,N):
        gamma_o = gamma_i + Ez[i]*np.cos(In_Phase)*L/m0 # don't know where this comes from
        beta_o = np.sqrt(1-1/gamma_o**2)
        twiss_i = twiss_o
        
        #calculate dE (derivative of field times dL)
        if i == 0:
            dE = Ez[1]/2 #Ez is zero at the edges of the cavity
        elif i == N-1:
            dE = Ez[N-2]/2
        else:
            dE = (Ez[i+1]-Ez[i-1])/2
        
        k = 1/(gamma_i*(beta_i**2)*m0)
        
        #coefficients from matrix in longitudinal phase space (Mzz)
        
        m21zz = k*dE*np.cos(In_Phase)
        m22zz = 1 - k*Ez[i]*L*np.cos(In_Phase)
        
        #coefficients from matrix in tranversal phase space (Mxx=Myy)
        
        m21xx = k*(-0.5*dE*np.cos(In_Phase)+beta_i*c*2*np.pi*f/(2*c**2)*Ez[i]*L*np.sin(In_Phase))
        m22xx = 1-k*Ez[i]*L*np.cos(In_Phase)
        
        if i < N-1:
            
            Mz1 = [[1, L/gamma_o**2], [m21zz, m22zz]]
            
            Mx1 = [[1, L], [m21xx, m22xx]]
            
            M0 = np.array([[0,0],[0,0]])
            M1 = np.concatenate((Mx1, M0, M0), axis = 1)
            M2 = np.concatenate((M0, Mx1, M0), axis = 1)
            M3 = np.concatenate((M0, M0, Mz1), axis = 1)
            M = np.concatenate((M1, M2, M3), axis = 0)
            
            twiss_o = twissTrM(twiss_i, M)
            betas_element[i,1:] = twiss_o[0,:]
            betas_element[i,0] = z_i + (i+1)*L
            
        else:
            Mz1 = np.dot([[1, L/gamma_o**2], [m21zz, m22zz]], np.linalg.inv([[1, L/gamma_o**2], [0, 1]]))
            
            Mx1 = np.dot([[1, L], [m21xx, m22xx]], np.linalg.inv([[1, L], [0, 1]]))
            
            M0 = np.array([[0,0],[0,0]])
            M1 = np.concatenate((Mx1, M0, M0), axis = 1)
            M2 = np.concatenate((M0, Mx1, M0), axis = 1)
            M3 = np.concatenate((M0, M0, Mz1), axis = 1)
            M = np.concatenate((M1, M2, M3), axis = 0)
            
            twiss_o = twissTrM(twiss_i, M)
            betas_element[i,1:] = twiss_o[0,:]
            #print(f"betas_element[i,1:] = {betas_element[i,1:]}")
            betas_element[i,0] = z_i + (i+1)*L
            #print(f"betas_element[i,0] = {betas_element[i,0]}")
        
        Mz = np.dot(Mz1,Mz)
        Mx = np.dot(Mx1,Mx)
        
        d_Phy = 2*np.pi*f*L/(beta_o*c)
        In_Phase = In_Phase + d_Phy
        
        if i < N-1:
            Phase_Shift = Phase_Shift+d_Phy
        
        gamma_i = gamma_o
        beta_i = beta_o
    
    Output_Energy = (gamma_o-1)*m0 
    
    #convert Phase_shift to degrees
    if elementType == 1:
        Phase_Shift = Phase_Shift*360/(2*np.pi)
    else: #already checked to that elementType was 1,2 or 3, can therefore use only "else"
        Phase_Shift = Phase_Shift*360/(2*np.pi)/2
        
    My = Mx
    
    M0 = np.zeros((2,2))
    #M = np.array([[Mx, M0, M0], [M0, My, M0], [M0, M0, Mz]])
    M1 = np.concatenate((Mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, My, M0), axis = 1)
    M3 = np.concatenate((M0, M0, Mz), axis = 1)
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    return Phase_Shift, Output_Energy, M, betas_element, twiss_o

def twiss_beta_from_lattice(Ref_Phase, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL, twiss_i_0):
    """
    A function that calculates the twiss beta going through the lattice defined by Ref_Phase. Every drift and wuad will be divided into 100 points and every cavity into as many points as the number of points in the Filed Map.
    
    Input:
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: the field map of a HBL cavity
        - twiss_i: 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z.
                
    Output:
        - betas = a matrix with four columns where each column is the position in m, beta_x, beta_y and beta_z. The betas bering the twiss paramenters. 
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    nbrElements = len(Ref_Phase[:,0])
    z_i = Ref_Phase[0,8]- Ref_Phase[0,1]/1000
    betas = np.array([[z_i, twiss_i_0[0,0], twiss_i_0[0,1], twiss_i_0[0,2]]])
    
    twiss_o = twiss_i_0
    #print(f"twiss_i_0 = {twiss_i_0}")
    
    for i in range(nbrElements):
        
        elementType = Ref_Phase[i,0]
        twiss_i = twiss_o
        
        if elementType == 4: #drift
            L = Ref_Phase[i, 1]/1000 #mm to m
            z = Ref_Phase[i, 8]
            z_i = z-L
            # multiply w betagamma
            gamma = 1 + Ref_Phase[i,5]/m0 #input energy = output energy
            betas_element, twiss_o = DRIFT_dl(L, 100, twiss_i, z_i, gamma)
            
            relBeta = np.sqrt(1-1/gamma**2)
            betagamma = gamma*relBeta
            #betas_element[:,1:] = betas_element[:,1:]*betagamma
            
            betas = np.append(betas, betas_element, axis=0)
            
        elif elementType == 5: #quad
            L = Ref_Phase[i,1]/1000 #mm to m
            z = Ref_Phase[i,8]
            z_i = z-L
            #print(f"z_i = {z_i}")
            Input_Energy = Ref_Phase[i,5]
            G = Ref_Phase[i,2]
            gamma = 1 + Input_Energy/m0
            beta = np.sqrt(1-1/gamma**2)
            Bro = m0*1e+6/c*beta*gamma #magnetic rigidity from P/q = m0 gamma beta c /e = E0 10^6 gamma beta/c if E0 is given in MeVs
            K = G/Bro
            betas_element, twiss_o = QUADK_dl(L, K, 100, twiss_i, z_i, gamma)
            
            # multiply w betagamma
            betagamma = gamma*beta
            #betas_element[:,1:] = betas_element[:,1:]*betagamma
            
            betas = np.append(betas, betas_element, axis=0)
            
            
        elif elementType == 1 or elementType == 2 or elementType == 3:
            AMP = Ref_Phase[i,2]
            Input_Phase = Ref_Phase[i,3]
            Input_Energy = Ref_Phase[i,5]
            L = Ref_Phase[i,1]/1000 #convert to m
            z_i = Ref_Phase[i,8]-L
            if elementType ==1:
                Field_Map = Field_Map_SPK
            elif elementType ==2:
                Field_Map = Field_Map_MBL
            else:
                Field_Map = Field_Map_HBL
                
            #using the matrix of a accelerating cavity
            Phase_Shift, Output_Energy, M, betas_element, twiss_o = Cavity_Matrix_Input_Phase_twiss(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i, z_i) 
            
            # multiply w betagamma
            #gamma = 1 + (Ref_Phase[i,6]+Ref_Phase[i,5])/(2*m0) #average of input and output energy
            gamma = 1 + Ref_Phase[i,6]/m0 #just using the output energy
            relBeta = np.sqrt(1-1/gamma**2)
            betagamma = gamma*relBeta
            #betas_element[:,1:] = betas_element[:,1:]*betagamma
                
            #twiss_o = twissTrM(twiss_i, M) #HÄR FELET ÄR KANSKE??
            betas = np.append(betas, betas_element, axis=0)
            
        #elif elementType == 1 or elementType == 2 or elementType == 3:
        #elif elementType == 10:
            #print("does this code ever run?")
            #AMP = Ref_Phase[i,2]
            #Input_Energy = Output_Energy #the energy the particle has coming out from the previous element
            #Input_Energy = Ref_Phase[i,5]
            #L = Ref_Phase[i,1]/1000 #convert to m
            #z = Ref_Phase[i,8]
            #Input_Phase = Ref_Phase[i,3]
            #z_i = z - L
            #
            #if elementType ==1:
            #    Field_Map = Field_Map_SPK
            #elif elementType ==2:
            #    Field_Map = Field_Map_MBL
            #else:
            #    Field_Map = Field_Map_HBL
            #
            #betas_element = [[0,0,0,0]]
            #using the matrix of a accelerating cavity
            #Phase_Shift, Output_Energy, M, betas_element, twiss_o = Cavity_Matrix_Input_Phase_twiss(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i, z_i)
            #
            #Phase_Shift, Output_Energy, M, betas_element, twiss_o = Cavity_Matrix_Input_Phase_twiss(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i, z_i) 
            #
            #print(f"size  betas before adding cavity {betas.shape}")
            #temp = betas.shape
            #betas = np.append(betas, betas_element, axis=0)
            #cavity_betas = np.append(cavity_betas, betas_element, axis=0)
            #print(f"size change of betas after adding cavity {betas.size-temp}")
            #print(f"size of betas after adding cavitys {betas.shape}")
        else:
            print(f"twiss_beta_from_lattice: wrong element type defined in row {i} of Ref_Phase, the type should be 1,2,3,4 or 5 but is {elementType}")
    return betas


def phase_advance_from_beta(betas, Ref_Phase, lengths):
    """
    A function that calculated the phase advance for the lattice defined by Ref_Phase. It calculates the phase advace from focusing quadrupole to focusing quadrupole. (beam phase advance)
    
    Input:
        - betas: a matrix with four columns where each column is the position in m, beta_x, beta_y and beta_z. The betas bering the twiss paramenters.
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - length: a vector containg how many points of beta each element has [SPK, MBL, HBL, drift, quad]
                            
    Output:
        -phase_advance: a 2xnbrCells matrix where the first column is the end-coodinates in z of the cells and the column is the calculated phase advances
    """
    c = cnst.c
    m0 = cnst.m0
    p = 0
    
    #calculate the rows of the cells
    nbrCells = 0
    for i in range(len(Ref_Phase[:,0])):
        elementType = Ref_Phase[i,0]
        K = Ref_Phase[i,2]
        if elementType == 5 and K >0: #is is a focusing quad
            if nbrCells == 0:
                #first column is the start of each cell in ref_phase and the second column in the start of each cell in betas.
                cellsIdx = [[i, 0]]
                nbrCells += 1
            else:
                cellsIdx = np.append(cellsIdx, [[i, 0]], axis = 0)
                nbrCells +=1
    
    #note that betas is len(ref_phase)+1
#    firstIdx = cellsIdx[0,0]
#    betaStartIdx = 1 #because the first row is the intitial conditons
#    i = 0
#    while(i != firstIdx):
#        elementType = Ref_Phase[i,0]
#        betaStartIdx += lengths[int(elementType)-1]
#        i += 1
#    cellsIdx[0,1] = firstIdx
    
    for cell in range(nbrCells):
        refIdx = cellsIdx[cell,0]
        if cell == 0:
            betaIdx = 1 #first row is the initil conditions
            element = 0
        else:
            betaIdx = cellsIdx[cell-1,1]
            element = cellsIdx[cell-1,0]
        while(element != refIdx):
            elementType = Ref_Phase[element,0]
            betaIdx += lengths[int(elementType)-1]
            element += 1
        cellsIdx[cell,1] = betaIdx
    #how can i check so this is correct? 
    
    #or should I start in the center of a focusing quad?
    
    phase_adv = np.zeros((nbrCells-1,4))
    
    qPoints = lengths[4]
    halfQuad = int(qPoints/2)
    
    for cell in range(nbrCells-1):
        startBeta = cellsIdx[cell,1]
        endBeta = cellsIdx[cell+1,1]
        cellBetas = betas[startBeta+halfQuad:endBeta+halfQuad] #to start in center of focusing quad
        zz = cellBetas[:,0]
        Advx = re_func.trapz(np.reciprocal(cellBetas[:,1]), zz) #reciprocal inverts every element in a vector
        Advy = re_func.trapz(np.reciprocal(cellBetas[:,2]), zz)
        Advz = re_func.trapz(np.reciprocal(cellBetas[:,3]), zz)
        
        #convert to degrees
        Advx = Advx*360/(2*np.pi)
        Advy = Advy*360/(2*np.pi)
        Advz = Advz*360/(2*np.pi)
        
        #multiply w factor for experimenting
        startIdx = cellsIdx[cell,0]
        #Input_Energy = Ref_Phase[startIdx,5]
        #gamma_i = 1 + Input_Energy/m0
        #beta_i = np.sqrt(1-1/gamma_i**2)
        
        endIdx = cellsIdx[cell+1,0]
        #Output_Energy = Ref_Phase[endIdx,6]
        #gamma_o = 1 + Output_Energy/m0
        #beta_o = np.sqrt(1-1/gamma_o**2)
        
        #factor = np.sqrt(beta_o*gamma_o)
        #factor = beta_o*gamma_o
        #Advx = Advx/factor
        #Advy = Advy/factor
        #Advz = Advz/factor
        #if p == 0:
        #    print(f"every phase advance value was divided by betagamma_o in phase_advance_from_beta")
        #    p = 1
        
        L = sum(Ref_Phase[startIdx:endIdx+1,1])/1000
        #print(f"In phase_advance_from_beta L = {L}")
        
        Advx = Advx/L
        Advy = Advy/L
        Advz = Advz/L
        
        phase_adv[cell,:] = [zz[-1], Advx, Advy, Advz]
    
    
    return phase_adv
    
    
def twiss_beta_from_Lattice_1perELement(Ref_Phase, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, twiss_i_0):

    """ A function that calculates the twiss beta going through the lattice defined by Ref_Phase. One value per element.
    
    Input:
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: the field map of a HBL cavity
        - twiss_i_0: 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z.
                
    Output:
        - betas = a matrix with four columns where each column is the position in m, beta_x, beta_y and beta_z. The betas being the twiss paramenters. 
    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    N = len(Ref_Phase[:,0])
    #Phase_adv = np.zeros((N,3))
    
    betas = np.zeros((N+1,4))
    
    twiss_o = twiss_i_0
    betas[0,:] = [Ref_Phase[0,8]-Ref_Phase[0,1]/1000, twiss_i_0[0,0], twiss_i_0[0,1], twiss_i_0[0,2]]
    
    gamma = 1 + Ref_Phase[0,5]/m0
    relBeta = np.sqrt(1-1/gamma**2)
    betas[0,1:] = betas[0,1:]*gamma*relBeta
    
    for i in range(N):
        twiss_i = twiss_o
        elementType = Ref_Phase[i,0]
        #print(f"i = {i} has elementType = {elementType}")
        
        if elementType == 4: #drift
            Input_Energy = Ref_Phase[i,5]
            L = Ref_Phase[i,1]/1000
            z = Ref_Phase[i,8]
            Output_Energy = Ref_Phase[i,6]
            
            gamma_i = 1 + Input_Energy/m0
            gamma_o = 1 + Output_Energy/m0
            d_Phy, Output_Energy,M = re_func.Drift_Matrix(Input_Energy,L)
            twiss_o = twissTrM_E(twiss_i, M, gamma_i, gamma_o)
            
            betas[i+1,0] = z
            betas[i+1,1]=twiss_o[0,0]
            betas[i+1,2]=twiss_o[0,1]
            betas[i+1,3]=twiss_o[0,2]
            
            
            #Multiply by a the betagamma factor
            gamma = 1 + Output_Energy/m0 #input energy = output energy
            relBeta = np.sqrt(1-1/gamma**2)
            betagamma = gamma*relBeta
            #betas[i+1,1:] = betas[i+1,1:]*betagamma
         
        elif elementType == 5: #quad
            Input_Energy = Ref_Phase[i,5]
            L = Ref_Phase[i,1]/1000
            G = Ref_Phase[i,2]
            z = Ref_Phase[i,8]
            Output_Energy = Ref_Phase[i,6]
            
            d_Phy, Output_Energy, M = re_func.Quad_Matrix(Input_Energy,L,G)
            
            gamma_i = 1 + Input_Energy/m0
            gamma_o = 1 + Output_Energy/m0
            twiss_o = twissTrM_E(twiss_i, M, gamma_i, gamma_o)
            
            betas[i+1,0] = z
            betas[i+1,1]=twiss_o[0,0]
            betas[i+1,2]=twiss_o[0,1]
            betas[i+1,3]=twiss_o[0,2]
            
            
            #Multiply by a the betagamma factor
            gamma = 1 + Output_Energy/m0 #input energy = output energy
            relBeta = np.sqrt(1-1/gamma**2)
            betagamma = gamma*relBeta
            #betas[i+1,1:] = betas[i+1,1:]*betagamma
            
        elif elementType == 1 or elementType == 2 or elementType == 3: #cavity
            Input_Energy = Ref_Phase[i,5]
            AMP = Ref_Phase[i,2]
            Input_Phase = Ref_Phase[i,3]
            L = Ref_Phase[i,1]/1000 #convert to m
            z = Ref_Phase[i,8]
            
            if elementType ==1:
                Field_Map = FIELD_MAP_SPK
            elif elementType ==2:
                Field_Map = FIELD_MAP_MBL
            else:
                Field_Map = FIELD_MAP_HBL
            
            Phase_Shift, Output_Energy, M = re_func.Cavity_Matrix_Input_Phase(elementType, Input_Phase, Input_Energy, AMP, Field_Map)
            gamma_i = 1 + Input_Energy/m0
            gamma_o = 1 + Output_Energy/m0
            twiss_o = twissTrM_E(twiss_i, M, gamma_i, gamma_o)
            
            betas[i+1,0] = z
            betas[i+1,1]=twiss_o[0,0]
            betas[i+1,2]=twiss_o[0,1]
            betas[i+1,3]=twiss_o[0,2]
            
            
            #Multiply by a the betagamma factor
            gamma = 1 + Output_Energy/m0 #input energy  (((((perhaps use average of input anf output energy?)))))
            relBeta = np.sqrt(1-1/gamma**2)
            betagamma = gamma*relBeta
            #betas[i+1,1:] = betas[i+1,1:]*betagamma
        else:
            print(f"Wrong type of element in twiss_beta_from_Lattice_1perELement for i = {i}")
            
    
    return betas


# calculate phase advance from transfer Matrix. Need to first determine the cells. Should this be done in a separate function?
def FFcells(Ref_Phase):
    """
    A function that determines the indexes of the cells of the lattice defined by Ref_Phase.
    A cell is defined as focusing quadrupole to next focusing quadrupole.
    
    Input:
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
                            
    Output:
        - cellsIdx: The index of all focusing quadrupoles in Ref_Phase
    """
    nbrCells = 0
    
    for i in range(len(Ref_Phase[:,0])):
        elementType = Ref_Phase[i,0]
        G = Ref_Phase[i,2]
        if elementType == 5 and G >0: #is is a focusing quad
            if nbrCells == 0:
                #first column is the start of each cell in ref_phase and the second column in the start of each cell in betas.
                cellsIdx = [i]
                nbrCells += 1
            else:
                cellsIdx = np.append(cellsIdx, [i], axis = 0)
                nbrCells +=1
                
    return cellsIdx

def phase_adv_from_M_lattice(Ref_Phase_0, cellsIdx, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL, twiss_i_0):
    """
    A function that calculated the phase advace from the transfermatrix of each cell. (Structural phase advance)
    
    Input:
         - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - cellsIdx: The index of all focusing quadrupoles in Ref_Phase
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: the field map of a HBL cavity
        
        
    Output:
        - Phase_Adv: a 4xN vector where N is the number of cells. The first column is the end position of the cell and the three other columns are the phase advace in x, y and z space
    """
    c = cnst.c
    m0 = cnst.m0
    p = 0
    
    #just so that I don't change in the actual Ref_Phase
    
    Ref_Phase = Ref_Phase_0.copy()
    
    firstQF = cellsIdx[0]
    if firstQF!=0:
        for i in range(firstQF):
            M = re_func.Lattice_2_Matrix(Ref_Phase[0:firstQF], Field_Map_SPK, Field_Map_MBL, Field_Map_HBL) 
            input_Energy = Ref_Phase[0,5]
            gamma_i = 1 + input_Energy/m0
            output_Energy = Ref_Phase[firstQF,6]
            gamma_o = 1 + output_Energy/m0
            twiss_o = twissTrM_E(twiss_i_0, M, gamma_i, gamma_o)
    else:
        twiss_o = twiss_i_0
    
    nbrCells = len(cellsIdx)-1
    Phase_Adv = np.zeros((nbrCells, 4))
    
    Ref_Phase[cellsIdx[0],1] = Ref_Phase[cellsIdx[0],1]/2
    for cell in range(nbrCells):
        twiss_i = twiss_o
        #build the transfer matrix
        #to go though half od the focusing quad at the start and half at the end of a cell
        start = cellsIdx[cell]
        stop = cellsIdx[cell+1]
        Ref_Phase[stop,1] = Ref_Phase[stop,1]/2 
        M = re_func.Lattice_2_Matrix(Ref_Phase[start:stop+1], Field_Map_SPK, Field_Map_MBL, Field_Map_HBL) #+1 to actually include end idx
        #twiss_o = twissTrM(twiss_i,M)
        input_Energy = Ref_Phase[start,5]
        gamma_i =1+input_Energy/m0
        output_Energy = Ref_Phase[stop,6]
        gamma_o = 1+output_Energy/m0
        twiss_o = twissTrM_E(twiss_i, M, gamma_i, gamma_o)
        
        #calculate phase advance
        twiss_beta_i = twiss_i[0,:]
        twiss_beta_o = twiss_o[0,:]
        
        #Input_Energy = Ref_Phase[start, 5]
        #Output_Energy = Ref_Phase[stop, 6]
        #gamma_i = 1 + Input_Energy/m0
        beta_i = np.sqrt(1-1/gamma_i**2)
        betagamma_i = beta_i*gamma_i
        #gamma_o = 1 + Output_Energy/m0
        beta_o = np.sqrt(1-1/gamma_o**2)
        betagamma_o = beta_o*gamma_o
        
        L = sum(Ref_Phase[start:stop+1,1])/1000
        #print(f"In phase_advance_from_M_lattice L = {L}")
        
        values = phase_adv_from_M(M, twiss_beta_i, twiss_beta_o, betagamma_i, betagamma_o, L)
        
        #multiply w factor for experimenting
        #factor = np.sqrt(betagamma_o)
        #factor = betagamma_o
        #values = values/factor
        #if p == 0:
        #    print(f"every phase advance value was divided by betagamma_o in phase_advance_from_M_lattice")
        #    p = 1
        
        #Already converted to degrees!
        #values = (360/(2*np.pi))*values
        
        #save phase advance in vector
        Phase_Adv[cell, :] = [Ref_Phase[stop,8], values[0], values[1], values[2]]
        
    return Phase_Adv
        
def twissTrM_E(twiss_i, M, gamma_i, gamma_o):
    """
    A function that preforms the transfromation from twiss_i to twiss_f casued by the element with
    the 6D transfer matrix M. The transfer Matrix is multiplied w a factor proportional to the change
    in energy.
    
    Input:
        - twiss_i: a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values before entering the section defined by M.
        - M: a 6D transfer matrix of a lattice section or an element.
        - gamma_i: the relativistic gamma before M
        - gamma_o: the relativistic gamma after M
    
    Output:
        - twiss_o: a 3x3 matrix containing the twiss parameters efter passing through the section defined by M.
                The same arrangement of the parameters as twiss_i
    """
    c = cnst.c
    m0 = cnst.m0
    
    twiss_o = np.zeros((3,3))
    
    for i in range(3):
        Mpart = M[i*2:i*2+2, i*2:i*2+2]
        #MInv = np.linalg.inv(Mpart) Not nessesary, looked at the difinition agan
        #t22 = MInv[0,0]
        #t12 = -MInv[0,1]
        #t21 = -MInv[1,0]
        #t11 = MInv[1,1]
        
        #gamma_i = 1 + E_i/m0
        beta_i = np.sqrt(1-1/gamma_i**2)
        
        #gamma_o = 1 + E_o/m0
        beta_o = np.sqrt(1-1/gamma_o**2)
        
        factor = np.sqrt((beta_o*gamma_o)/(beta_i*gamma_i))
        
        if i == 2:
            #factor = 2*(beta_o*gamma_o**3)
            factor = 1
            
        t11 = Mpart[0,0]*factor
        t12 = Mpart[0,1]*factor
        t21 = Mpart[1,0]*factor
        t22 = Mpart[1,1]*factor
        
        MTwiss = [[t11**2, -2*t11*t12, t12**2], [-t11*t21, t11*t22+t12*t21, -t12*t22], [t21**2, -2*t21*t22, t22**2]]
        #print("MTwiss")
        #print(MTwiss)
        
        Twiss_i = twiss_i[:,i]
        Twiss_i = np.reshape(Twiss_i, (3,1))
        #print("Twiss_i: " + str(Twiss_i))
        Twiss_f = MTwiss@Twiss_i
        #print("Twiss_f: " + str(Twiss_f))
        
        #if Twiss_f[0] < 0:
        #    print("error in TwissTrM, negative beta for dimension " + str(i))
            
        for j in range(3):
            twiss_o[j,i] = Twiss_f[j] #ugly solution but it did not want to cooperate

        # beta/gamma must always be above 0:
        for i in [0,2]: 
            if(twiss_o[i] < 0).any():
                raise ValueError(f"Calculated wrong Twiss matrix in column {i}: {twiss_o}")

    return twiss_o

def twiss_beta_from_lattice_E(Ref_Phase, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL, twiss_i_0):
    """
    A function that calculates the twiss beta going through the lattice defined by Ref_Phase. Every drift and wuad will be divided into 100 points and every cavity into as many points as the number of points in the Field Map.
    
    Input:
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: the field map of a HBL cavity
        - twiss_i: 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z.
                
    Output:
        - betas = a matrix with four columns where each column is the position in m, beta_x, beta_y and beta_z. The betas bering the twiss paramenters. 
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    nbrElements = len(Ref_Phase[:,0])
    z_i = Ref_Phase[0,8]- Ref_Phase[0,1]/1000
    betas = np.array([[z_i, twiss_i_0[0,0], twiss_i_0[0,1], twiss_i_0[0,2]]])
    
    twiss_o = twiss_i_0
    #print(f"twiss_i_0 = {twiss_i_0}")
    
    for i in range(nbrElements):
        
        elementType = Ref_Phase[i,0]
        twiss_i = twiss_o
        
        if elementType == 4 or elementType == 0: #drift
            L = Ref_Phase[i, 1]/1000 #mm to m
            z = Ref_Phase[i, 8]
            z_i = z-L
            # multiply w betagamma
            gamma = 1 + Ref_Phase[i,5]/m0 #input energy = output energy
            betas_element, twiss_o = DRIFT_dl(L, 100, twiss_i, z_i, gamma)
            
            
            relBeta = np.sqrt(1-1/gamma**2)
            factor = 2*gamma**3*relBeta
            #factor = gamma**2
            betas_element[:,1:] = betas_element[:,1:]*factor
            
            betas = np.append(betas, betas_element, axis=0)
            
        elif elementType == 5: #quad
            L = Ref_Phase[i,1]/1000 #mm to m
            z = Ref_Phase[i,8]
            z_i = z-L
            #print(f"z_i = {z_i}")
            Input_Energy = Ref_Phase[i,5]
            G = Ref_Phase[i,2]
            gamma = 1 + Input_Energy/m0
            beta = np.sqrt(1-1/gamma**2)
            Bro = m0*1e+6/c*beta*gamma #magnetic rigidity from P/q = m0 gamma beta c /e = E0 10^6 gamma beta/c if E0 is given in MeVs
            K = G/Bro
            betas_element, twiss_o = QUADK_dl(L, K, 100, twiss_i, z_i, gamma)
            
            
            factor = 2*gamma**3*beta
            #factor = gamma**2
            betas_element[:,1:] = betas_element[:,1:]*factor
            
            betas = np.append(betas, betas_element, axis=0)
            
            
        elif elementType == 1 or elementType == 2 or elementType == 3:
            AMP = Ref_Phase[i,2]
            Input_Phase = Ref_Phase[i,3]
            Input_Energy = Ref_Phase[i,5]
            L = Ref_Phase[i,1]/1000 #convert to m
            z_i = Ref_Phase[i,8]-L
            if elementType ==1:
                Field_Map = Field_Map_SPK
            elif elementType ==2:
                Field_Map = Field_Map_MBL
            else:
                Field_Map = Field_Map_HBL
                
            #using the matrix of a accelerating cavity
            Phase_Shift, Output_Energy, M, betas_element, twiss_o = Cavity_Matrix_Input_Phase_twiss_E(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i, z_i) 
            
            # multiply w betagamma
            #gamma = 1 + (Ref_Phase[i,6]+Ref_Phase[i,5])/(2*m0) #average of input and output energy
            gamma = 1 + Ref_Phase[i,6]/m0 #just using the output energy
            relBeta = np.sqrt(1-1/gamma**2)
            
            factor = 2*gamma**3*relBeta
            #factor = gamma**2
            betas_element[:,1:] = betas_element[:,1:]*factor
            betas = np.append(betas, betas_element, axis=0)
        else:
            print(f"twiss_beta_from_lattice: wrong element type defined in row {i} of Ref_Phase, the type should be 1,2,3,4 or 5 but is {elementType}")
    return betas

def twiss_beta_from_lattice_Egamma(Ref_Phase, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL, twiss_i_0):
    """
    A function that calculates the twiss beta going through the lattice defined by Ref_Phase. Every drift and wuad will be divided into 100 points and every cavity into as many points as the number of points in the Field Map. Divides the longitudinal beta with gamma^2
    
    Input:
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: the field map of a HBL cavity
        - twiss_i: 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z.
                
    Output:
        - betas = a matrix with four columns where each column is the position in m, beta_x, beta_y and beta_z. The betas bering the twiss paramenters. 
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    nbrElements = len(Ref_Phase[:,0])
    z_i = Ref_Phase[0,8]- Ref_Phase[0,1]/1000
    betas = np.array([[z_i, twiss_i_0[0,0], twiss_i_0[0,1], twiss_i_0[0,2]]])
    
    twiss_o = twiss_i_0
    #print(f"twiss_i_0 = {twiss_i_0}")
    
    for i in range(nbrElements):
        
        elementType = Ref_Phase[i,0]
        twiss_i = twiss_o
        
        if elementType == 4: #drift
            L = Ref_Phase[i, 1]/1000 #mm to m
            z = Ref_Phase[i, 8]
            z_i = z-L
            # multiply w betagamma
            gamma = 1 + Ref_Phase[i,5]/m0 #input energy = output energy
            betas_element, twiss_o = DRIFT_dl(L, 100, twiss_i, z_i, gamma)
            
            
            relBeta = np.sqrt(1-1/gamma**2)
            betagamma = gamma*relBeta
            #betas_element[:,1:] = betas_element[:,1:]*betagamma
            
            #divide the longitudinal beta w gamma^2
            #betas_element[:,3] = betas_element[:,3]*relBeta*gamma**3
            #betas_element[:,3] = betas_element[:,3]*relBeta*gamma
            
            betas = np.append(betas, betas_element, axis=0)
            
        elif elementType == 5: #quad
            L = Ref_Phase[i,1]/1000 #mm to m
            z = Ref_Phase[i,8]
            z_i = z-L
            #print(f"z_i = {z_i}")
            Input_Energy = Ref_Phase[i,5]
            G = Ref_Phase[i,2]
            gamma = 1 + Input_Energy/m0
            relBeta = np.sqrt(1-1/gamma**2)
            Bro = m0*1e+6/c*relBeta*gamma #magnetic rigidity from P/q = m0 gamma beta c /e = E0 10^6 gamma beta/c if E0 is given in MeVs
            K = G/Bro
            betas_element, twiss_o = QUADK_dl(L, K, 100, twiss_i, z_i, gamma)
            
            # multiply w betagamma
            betagamma = gamma*relBeta
            #betas_element[:,1:] = betas_element[:,1:]*betagammaf
            
            
            #divide the longitudinal beta w gamma^2
            #betas_element[:,3] = betas_element[:,3]*relBeta*gamma**3
            #betas_element[:,3] = betas_element[:,3]*relBeta*gamma
            
            
            betas = np.append(betas, betas_element, axis=0)
            
            
        elif elementType == 1 or elementType == 2 or elementType == 3:
            AMP = Ref_Phase[i,2]
            Input_Phase = Ref_Phase[i,3]
            Input_Energy = Ref_Phase[i,5]
            L = Ref_Phase[i,1]/1000 #convert to m
            z_i = Ref_Phase[i,8]-L
            if elementType ==1:
                Field_Map = Field_Map_SPK
            elif elementType ==2:
                Field_Map = Field_Map_MBL
            else:
                Field_Map = Field_Map_HBL
                
            #using the matrix of a accelerating cavity
            #Phase_Shift, Output_Energy, M, betas_element, twiss_o = Cavity_Matrix_Input_Phase_twiss_ElongFactor(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i, z_i) 
            Phase_Shift, Output_Energy, M, betas_element, twiss_o = Cavity_Matrix_Input_Phase_twiss_E(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i, z_i) 
            
            # multiply w betagamma
            #gamma = 1 + (Ref_Phase[i,6]+Ref_Phase[i,5])/(2*m0) #average of input and output energy
            gamma = 1 + Ref_Phase[i,6]/m0 #just using the output energy
            relBeta = np.sqrt(1-1/gamma**2)
            betagamma = gamma*relBeta
            #betas_element[:,1:] = betas_element[:,1:]*betagamma
            
            
            #divide the longitudinal beta w gamma^2
            #betas_element[:,3] = betas_element[:,3]*relBeta*gamma**3
            #betas_element[:,3] = betas_element[:,3]*relBeta*gamma
            
            betas = np.append(betas, betas_element, axis=0)
        else:
            print(f"twiss_beta_from_lattice: wrong element type defined in row {i} of Ref_Phase, the type should be 1,2,3,4 or 5 but is {elementType}")
    return betas

def Cavity_Matrix_Input_Phase_twiss_E(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i_0, z_i):
    """ 
    A function that decribes the particles movement in an accelerating cavity, with the twiss parameters included.
    Calculated the energy in every step of the way.
    
    Input:
         - elementType: 1 = spoke, 2 = MBL and 3 = HBL
         - Input_Phase: defined by the lattice file (WHAT DOES THIS MEAN? Not defined by the abs_phase before?) [degrees]
         - Input_Energy: the energy of the particle enetring this cavity [MeV]
         - AMP: how much of the maximum power the cavity is running at, set in the lattice file.
         - Field_Map: the field_map of the relevant cavity running at the maximum power. Must be an array. [z (m), Ex (MV/m), Ey (MV/m), Ez (MV/m)]
    
    Output:
         - Phase_Shift: The phase change of the beam as a result from travveling in the cavity [degrees]
         - Output_Energy: The energy of the beam a result of travelling in the cavity [MeV]
         - M: The matrix that descibes the cavities affect on the 6D phase-space of the beam [radian-m phase-space]
         - twiss_o = a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values after exiting the cavity.
    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    if elementType == 1:
        f = cnst.f #Hz
    elif elementType == 2 or elementType == 3:
        f = 2*cnst.f
    else:
        print(f"Not a cavity as elementType input to Cavity_Matrix_Input_Phase_twiss: {elementType}")
    
    Ez = Field_Map[:,3] # MV/m
    Ez = Ez*AMP
    z = Field_Map[:, 0] #m
    N = len(Ez)
    L_total = z[N-1]
    L = L_total/(N-1)
    
    betas_element = np.zeros((N,4))
    
    In_Phase = Input_Phase*2*np.pi/360 #convert phase to radians
    gamma_i = 1 + Input_Energy/m0 #m0 defined in 'variables'
    beta_i = np.sqrt(1-1/gamma_i**2)
    
    Mz = np.identity(2)
    Mx = np.identity(2)
    
    Phase_Shift = 0
    
    twiss_o = twiss_i_0
    
    for i in range(0,N):
        gamma_o = gamma_i + Ez[i]*np.cos(In_Phase)*L/m0 # don't know where this comes from
        beta_o = np.sqrt(1-1/gamma_o**2)
        twiss_i = twiss_o
        
        #calculate dE (derivative of field times dL)
        if i == 0:
            dE = Ez[1]/2 #Ez is zero at the edges of the cavity
        elif i == N-1:
            dE = Ez[N-2]/2
        else:
            dE = (Ez[i+1]-Ez[i-1])/2
        
        k = 1/(gamma_i*(beta_i**2)*m0)
        
        #coefficients from matrix in longitudinal phase space (Mzz)
        
        m21zz = k*dE*np.cos(In_Phase)
        m22zz = 1 - k*Ez[i]*L*np.cos(In_Phase)
        
        #coefficients from matrix in tranversal phase space (Mxx=Myy)
        
        m21xx = k*(-0.5*dE*np.cos(In_Phase)+beta_i*c*2*np.pi*f/(2*c**2)*Ez[i]*L*np.sin(In_Phase))
        m22xx = 1-k*Ez[i]*L*np.cos(In_Phase)
        
        if i < N-1:
            
            Mz1 = [[1, L/gamma_o**2], [m21zz, m22zz]]
            
            Mx1 = [[1, L], [m21xx, m22xx]]
            
            M0 = np.array([[0,0],[0,0]])
            M1 = np.concatenate((Mx1, M0, M0), axis = 1)
            M2 = np.concatenate((M0, Mx1, M0), axis = 1)
            M3 = np.concatenate((M0, M0, Mz1), axis = 1)
            M = np.concatenate((M1, M2, M3), axis = 0)
            
            twiss_o = twissTrM_E(twiss_i, M, gamma_i, gamma_o)
            betas_element[i,1:] = twiss_o[0,:]
            betas_element[i,0] = z_i + (i+1)*L
            
        else:
            Mz1 = np.dot([[1, L/gamma_o**2], [m21zz, m22zz]], np.linalg.inv([[1, L/gamma_o**2], [0, 1]]))
            
            Mx1 = np.dot([[1, L], [m21xx, m22xx]], np.linalg.inv([[1, L], [0, 1]]))
            
            M0 = np.array([[0,0],[0,0]])
            M1 = np.concatenate((Mx1, M0, M0), axis = 1)
            M2 = np.concatenate((M0, Mx1, M0), axis = 1)
            M3 = np.concatenate((M0, M0, Mz1), axis = 1)
            M = np.concatenate((M1, M2, M3), axis = 0)
            
            twiss_o = twissTrM_E(twiss_i, M, gamma_i, gamma_o)
            betas_element[i,1:] = twiss_o[0,:]
            #print(f"betas_element[i,1:] = {betas_element[i,1:]}")
            betas_element[i,0] = z_i + (i+1)*L
            #print(f"betas_element[i,0] = {betas_element[i,0]}")
        
        Mz = np.dot(Mz1,Mz)
        Mx = np.dot(Mx1,Mx)
        
        d_Phy = 2*np.pi*f*L/(beta_o*c)
        In_Phase = In_Phase + d_Phy
        
        if i < N-1:
            Phase_Shift = Phase_Shift+d_Phy
        
        gamma_i = gamma_o
        beta_i = beta_o
    
    Output_Energy = (gamma_o-1)*m0 
    
    #convert Phase_shift to degrees
    if elementType == 1:
        Phase_Shift = Phase_Shift*360/(2*np.pi)
    else: #already checked to that elementType was 1,2 or 3, can therefore use only "else"
        Phase_Shift = Phase_Shift*360/(2*np.pi)/2
        
    My = Mx
    
    M0 = np.zeros((2,2))
    #M = np.array([[Mx, M0, M0], [M0, My, M0], [M0, M0, Mz]])
    M1 = np.concatenate((Mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, My, M0), axis = 1)
    M3 = np.concatenate((M0, M0, Mz), axis = 1)
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    return Phase_Shift, Output_Energy, M, betas_element, twiss_o


def phase_advance_from_beta1(betas1, Ref_Phase, lengths, dim):
    """
    A function that calculated the phase advance for the lattice defined by Ref_Phase. It calculates the phase advace from focusing quadrupole to focusing quadrupole. (beam phase advance)
    
    Input:
        - betas1: a matrix with either three or two columns where each column is the position in m, beta_x and beta_y or beta_z. The betas being the twiss paramenters.
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - length: a vector containg how many points of beta each element has [SPK, MBL, HBL, drift, quad]
        - Rematch_Cavities_index: position of cavities to be rematched: column 1: type, column 2: absolute 
                                    position, column 3: relative position in current section
        - dim: 12 if transversal calculations and 3 if longitudidal calculations
                            
    Output:
        -phase_advance: a 2xnbrCells matrix where the first column is the end-coodinates in z of the cells and the column is the calculated phase advances
    """
    c = cnst.c
    m0 = cnst.m0
    p = 0
    
    #calculate the rows of the cells
    nbrCells = 0
    for i in range(len(Ref_Phase[:,0])):
        elementType = Ref_Phase[i,0]
        K = Ref_Phase[i,2]
        if elementType == 5 and K >0: #is is a focusing quad
            if nbrCells == 0:
                #first column is the start of each cell in ref_phase and the second column in the start of each cell in betas.
                cellsIdx = [[i, 0]]
                nbrCells += 1
            else:
                cellsIdx = np.append(cellsIdx, [[i, 0]], axis = 0)
                nbrCells +=1
    
    #note that betas is len(ref_phase)+1
#    firstIdx = cellsIdx[0,0]
#    betaStartIdx = 1 #because the first row is the intitial conditons
#    i = 0
#    while(i != firstIdx):
#        elementType = Ref_Phase[i,0]
#        betaStartIdx += lengths[int(elementType)-1]
#        i += 1
#    cellsIdx[0,1] = firstIdx
    
    for cell in range(nbrCells):
        refIdx = cellsIdx[cell,0]
        if cell == 0:
            betaIdx = 1 #first row is the initil conditions
            element = 0
        else:
            betaIdx = cellsIdx[cell-1,1]
            element = cellsIdx[cell-1,0]
        while(element != refIdx):
            elementType = Ref_Phase[element,0]
            betaIdx += lengths[int(elementType)-1]
            element += 1
        cellsIdx[cell,1] = betaIdx
    
    qPoints = lengths[4]
    halfQuad = int(qPoints/2)
    
    if dim == 12:
        phase_adv = np.zeros((nbrCells-1,3))
        for cell in range(nbrCells-1):
            startBeta = cellsIdx[cell,1]
            endBeta = cellsIdx[cell+1,1]
            cellBetas = betas1[startBeta+halfQuad:endBeta+halfQuad, :] #to start in center of focusing quad
            zz = cellBetas[:,0]
            Advx = re_func.trapz(np.reciprocal(cellBetas[:,1]), zz) #reciprocal inverts every element in a vector
            Advy = re_func.trapz(np.reciprocal(cellBetas[:,2]), zz)
            #convert to degrees
            Advx = Advx*360/(2*np.pi)
            Advy = Advy*360/(2*np.pi)
            
            #multiply w factor for experimenting
            startIdx = cellsIdx[cell,0]
            
            endIdx = cellsIdx[cell+1,0]
            
            L = sum(Ref_Phase[startIdx:endIdx+1,1])/1000
            #print(f"In phase_advance_from_beta L = {L}")
            
            Advx = Advx/L
            Advy = Advy/L
            
            phase_adv[cell,:] = [zz[-1], Advx, Advy]
    elif dim == 3:
        phase_adv = np.zeros((nbrCells-1,2))
        for cell in range(nbrCells-1):
            startBeta = cellsIdx[cell,1]
            endBeta = cellsIdx[cell+1,1]
            cellBetas = betas1[startBeta+halfQuad:endBeta+halfQuad] #to start in center of focusing quad
            zz = cellBetas[:,0]
            Adv = re_func.trapz(np.reciprocal(cellBetas[:,1]), zz) #reciprocal inverts every element in a vector
            
            #convert to degrees
            Adv = Adv*360/(2*np.pi)
            
            #multiply w factor for experimenting
            startIdx = cellsIdx[cell,0]
            
            endIdx = cellsIdx[cell+1,0]
            
            L = sum(Ref_Phase[startIdx:endIdx+1,1])/1000
            #print(f"In phase_advance_from_beta L = {L}")
            
            Adv = Adv/L
            
            phase_adv[cell,:] = [zz[-1], Adv]
    else:
        print(f"Wrong dim input into phase_advance_from_beta1, should be 12 or 3 but is: {dim}")
    
    return phase_adv


#later problem, rewrite all functions used to only calculate one dimension at a time
def twiss_beta_from_lattice_E1(Ref_Phase, Field_Map_SPK, Field_Map_MBL, Field_Map_HBL, twiss_i_0, dim):
    """
    A function that calculates the twiss beta going through the lattice defined by Ref_Phase. Every drift and wuad will be divided into 100 points and every cavity into as many points as the number of points in the Field Map.
    
    Input:
        - Ref_Phase: A matrix where each row contains data about one accelteret component in 10 columns:
                            Type (1 = SPK, 2 = MBL, 3 = HBL, 4 = drift, 5 = quad)
                            Length (mm)
                            gradient (T/m) or AMP (factor of power stored in Field Map)
                            Input_Phase (degree)
                            Sync_Phase (degree)
                            Input_energy (MeV)
                            Output_energy (MeV)
                            d_Phy (degree)
                            Full_Length (m)
                            abs_Phase (degree)
        - Field_Map_SPK: the field map of a SPK cavity
        - Field_Map_MBL: the field map of a MBL cavity
        - Field_Map_HBL: the field map of a HBL cavity
        - twiss_i_01: 3x1 matrix where the column is beta_i, alpha_i, gamma_i for the dimension I want to calculate
        - dim: either 12 for transversal calculations or 3 for longitudinal calculations.
    Output:
        - betas = a matrix with two columns where each column is the position in m, beta_i 
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    nbrElements = len(Ref_Phase[:,0])
    z_i = Ref_Phase[0,8]- Ref_Phase[0,1]/1000
    twiss_o = twiss_i_0
    
    if dim == 12:
        if (np.shape(twiss_i_0) == (3,2)) == False:
            print(f"wrong shape of twiss_i_o into twiss_beta_from_lattice_E1, is {np.shape(twiss_i_0)} should be (3,2)")
        betas = np.array([[z_i, twiss_i_0[0,0], twiss_i_0[0,1]]]) #start w just the initiacl value of the transversal beta
        #print(f"intial beta = {betas}")
    elif dim == 3:
        if (np.shape(twiss_i_0) == (3,) or np.shape(twiss_i_0) == (3,1)) == False:
            print(f"wrong shape of twiss_i_o into twiss_beta_from_lattice_E1, is {np.shape(twiss_i_0)} should be (3,2)")
        betas = np.array([[z_i, twiss_i_0[0]]]) #start w just the initiacl value of the longitudinal beta
    else:
        print(f"wrong dim input into twiss_beta_from_lattice_E1, dim = {dim}")
    
    
    #print(f"twiss_i_0 = {twiss_i_0}")
    
    for i in range(nbrElements):
        
        elementType = Ref_Phase[i,0]
        twiss_i = twiss_o
        
        if elementType == 4 or elementType == 0 : #drift failed cavity treated as drift
            L = Ref_Phase[i, 1]/1000 #mm to m
            z = Ref_Phase[i, 8]
            z_i = z-L
            # multiply w betagamma
            gamma = 1 + Ref_Phase[i,5]/m0 #input energy = output energy
            betas_element, twiss_o = DRIFT_dl1(L, 100, twiss_i, z_i, gamma, dim)
            
            relBeta = np.sqrt(1-1/gamma**2)
            betas_element[:,1:] = betas_element[:,1:]*2*relBeta*gamma**3
            
            betas = np.append(betas, betas_element, axis=0)
            
        elif elementType == 5: #quad
            L = Ref_Phase[i,1]/1000 #mm to m
            z = Ref_Phase[i,8]
            z_i = z-L
            #print(f"z_i = {z_i}")
            Input_Energy = Ref_Phase[i,5]
            G = Ref_Phase[i,2]
            gamma = 1 + Input_Energy/m0
            beta = np.sqrt(1-1/gamma**2)
            Bro = m0*1e+6/c*beta*gamma #magnetic rigidity from P/q = m0 gamma beta c /e = E0 10^6 gamma beta/c if E0 is given in MeVs
            K = G/Bro
            betas_element, twiss_o = QUADK_dl1(L, K, 100, twiss_i, z_i, gamma, dim)
            
            # multiply w betagamma
            betas_element[:,1:] = betas_element[:,1:]*2*relBeta*gamma**3
            
            betas = np.append(betas, betas_element, axis=0)
            
            
        elif elementType == 1 or elementType == 2 or elementType == 3:
            AMP = Ref_Phase[i,2]
            Input_Phase = Ref_Phase[i,3]
            Input_Energy = Ref_Phase[i,5]
            L = Ref_Phase[i,1]/1000 #convert to m
            z_i = Ref_Phase[i,8]-L
            if elementType ==1:
                Field_Map = Field_Map_SPK
            elif elementType ==2:
                Field_Map = Field_Map_MBL
            else:
                Field_Map = Field_Map_HBL
                
            #using the matrix of a accelerating cavity
            Phase_Shift, Output_Energy, M, betas_element, twiss_o = Cavity_Matrix_Input_Phase_twiss_E1(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i, z_i, dim) 
            
            gamma = 1 + Ref_Phase[i,6]/m0 #just using the output energy
            relBeta = np.sqrt(1-1/gamma**2)
            betas_element[:,1:] = betas_element[:,1:]*2*relBeta*gamma**3
            #twiss_o = twissTrM(twiss_i, M)
            betas = np.append(betas, betas_element, axis=0)
        else:
            print(f"twiss_beta_from_lattice: wrong element type defined in row {i} of Ref_Phase, the type should be 1,2,3,4 or 5 but is {elementType}")
    return betas

def DRIFT_dl1(L, n, twiss_i, z_i, gamma, dim):
    '''
    Calculates the beta values for n equidistant points in a drift space.
    
    Input:
        - L: float, Length of the drift 
        - n: int, number of steps in the drift 
        - twiss_i: a 3x2 matrix if dim = 12, and a 3x1 matrix if dim = 3, where each column is beta_i, alpha_i, gamma_i. And the three columns correpond to x and y or z. This matrix contains the initial values before entering the drift space.
        - z_i: The position of the entrance of the drift space.
        - gamma: The relativistic gamma of the particles enetering the drift space.
        - dim: either 12 for transversal calculations or 3 for longitudinal calculations.
    
    | 1    L |
    |        |
    | 0    1 | 
    
    Ouput:
        - betas_element: a 4xn matrix where each row is z, beta_x, beta_y and beta_z for n equidistant points in a drift space of length L.
        - twiss_o = a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values after exiting the drift space.
    
    '''
    
    #ele_type = 0
    dl = L/n
    
    betas_element = np.zeros((n,2))
    
    mx = np.array([[1, dl],[0, 1]])
    my = np.array([[1, dl],[0, 1]])
    mz = np.array([[1, dl/gamma**2],[0, 1]])
    M0 = np.array([[0,0],[0,0]])
    M1 = np.concatenate((mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, my, M0), axis = 1)
    M3 = np.concatenate((M0, M0, mz), axis = 1)
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    if dim == 12:
        if (np.shape(twiss_i) == (3,2)) ==False:
            print(f"wrong input shape of twiss_i to DRIFT_dl1, shape is : {np.shape(twiss_i)}, should be (3,2)")
        betas_element = np.zeros((n,3))
        for i in range(n):
            twiss_o = twissTrM_E1(twiss_i, M, gamma, gamma, dim)
            betas_element[i,1:] = twiss_o[0,:]
            betas_element[i,0] = z_i + (i+1)*dl
            twiss_i = twiss_o
    elif dim == 3:
        #check if twiss_i has the correct shape
        if (np.shape(twiss_i) == (3,) or np.shape(twiss_i) == (3,1)) ==False:
            print(f"wrong input shape of twiss_i to DRIFT_dl1, shape is : {np.shape(twiss_i)}, should be (3,) or (3,1)")
        betas_element = np.zeros((n,2))
        for i in range(n):
            twiss_o = twissTrM_E1(twiss_i, M, gamma, gamma, dim)
            betas_element[i,1] = twiss_o[0]
            betas_element[i,0] = z_i + (i+1)*dl
            twiss_i = twiss_o
    
    else:
        print("wrong dim input into DRIFT_dl1")

    return betas_element, twiss_o

def QUADK_dl1(L, K, n, twiss_i, z_i, gamma, dim):
    '''
    Calculates the twiss beta values for n equidistand points in a thick quadrupole using the normalized gradient
    
    Input: 
        - L: float, Length of the quadrupole 
        - K: float, normalized gradient
        - n: int, number of steps in the quadrupole
        - twiss_i: a 3x2 matrix if dim = 12, and a 3x1 matrix if dim = 3, where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values before entering the drift space.
        - z_i: The position of the entrance of the drift space.
        - gamma: The relativistic gamma of the particles enetering the drift space.
        - dim: either 12 for transversal calculations or 3 for longitudinal calculations.

    | cos(dl*K**0.5)             sin(dl*K**0.5)/K**0.5 |
    |                                                  |
    | -sin(dl*K**0.5)*K**0.5     cos(dl*K**0.5)        |
    

    | cosh(dl*K**0.5)           sinh(dl*K**0.5)/K**0.5 |
    |                                                  |
    | sinh(dl*K**0.5)*K**0.5    cosh(dl*K**0.5)        |    
    
    Output:
        - betas_element: a 4xn matrix where each row is z, beta_x, beta_y and beta_z for n equidistant points in a thick quadrupole of length L with the normalized field strength K.
        - twiss_o = a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values after exiting the quadrupole.

    '''

    mx=np.eye(2)
    my=np.eye(2)
    mz=np.eye(2)

    ele_type = 1
    ele_type *= np.sign(K)
    dl = L/n
    
    betas_element = np.zeros((n,4))
    
    mz[0,1] = dl/gamma**2

    if K > 0:
        mx[0,0] = np.cos(dl*K**0.5)
        mx[1,1] = mx[0,0]
        mx[0,1] = np.sin(dl*K**0.5)/K**0.5
        mx[1,0] = -K*mx[0,1]
        
        my[0,0] = np.cosh(dl*K**0.5)
        my[1,1] = my[0,0]
        my[0,1] = np.sinh(dl*K**0.5)/K**0.5
        my[1,0] = K*my[0,1]
        
    elif K < 0:
        K*=-1
        my[0,0] = np.cos(dl*K**0.5)
        my[1,1] = my[0,0]
        my[0,1] = np.sin(dl*K**0.5)/K**0.5
        my[1,0] = -K*my[0,1]

        mx[0,0] = np.cosh(dl*K**0.5)
        mx[1,1] = mx[0,0]
        mx[0,1] = np.sinh(dl*K**0.5)/K**0.5
        mx[1,0] = K*mx[0,1]
        K*=-1
    elif K == 0:
        mx[0,1] = my[0,1] = dl

    M0 = np.array([[0,0],[0,0]])
    M1 = np.concatenate((mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, my, M0), axis = 1)
    M3 = np.concatenate((M0, M0, mz), axis = 1)
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    if dim == 12:
        if (np.shape(twiss_i) == (3,2)) ==False:
            print(f"wrong input shape of twiss_i to QUAD_dl1, shape is : {np.shape(twiss_i)}, should be (3,2)")
        betas_element = np.zeros((n,3))
        for i in range(n):
            twiss_o = twissTrM_E1(twiss_i, M, gamma, gamma, dim)
            betas_element[i,1:] = twiss_o[0,:]
            betas_element[i,0] = z_i + (i+1)*dl
            twiss_i = twiss_o
    elif dim == 3:
        #check if twiss_i has the correct shape
        if (np.shape(twiss_i) == (3,) or np.shape(twiss_i) == (3,1)) ==False:
            print(f"wrong input shape of twiss_i to QUAD_dl1, shape is : {np.shape(twiss_i)}, should be (3,) or (3,1)")
        betas_element = np.zeros((n,2))
        for i in range(n):
            twiss_o = twissTrM_E1(twiss_i, M, gamma, gamma, dim)
            betas_element[i,1] = twiss_o[0]
            betas_element[i,0] = z_i + (i+1)*dl
            twiss_i = twiss_o
    
    else:
        print("wrong dim input into QUAD_dl1")
    
  
    return betas_element, twiss_o

def twissTrM_E1(twiss_i, M, gamma_i, gamma_o, dim):
    """
    A function that preforms the transfromation from twiss_i to twiss_f casued by the element with
    the 6D transfer matrix M. The transfer Matrix is multiplied w a factor proportional to the change
    in energy.
    
    Input:
        - twiss_i: a 3x2 matrix if dim = 12, and a 3x1 matrix if dim = 3, where each column is beta_i, alpha_i, gamma_i. And the three columns correpond to x and y  or only z. This matrix contains the initial values before entering the section defined by M.
        - M: a 6D transfer matrix of a lattice section or an element.
        - gamma_i: the relativistic gamma before M
        - gamma_o: the relativistic gamma after M
        - dim: either 12 for transversal calculations or 3 for longitudinal calculations.
    
    Output:
        - twiss_o: a 3x2 matrix if dim = 12, and a 3x1 matrix if dim = 3 containing the twiss parameters efter passing through the section defined by M. The same arrangement of the parameters as twiss_i
    """
    c = cnst.c
    m0 = cnst.m0
    
    if dim == 12:
        twiss_o = np.zeros((3,2))
        
        for i in range(2):
            Mpart = M[i*2:i*2+2, i*2:i*2+2]
            
            #gamma_i = 1 + E_i/m0
            beta_i = np.sqrt(1-1/gamma_i**2)
            
            #gamma_o = 1 + E_o/m0
            beta_o = np.sqrt(1-1/gamma_o**2)
            
            factor = np.sqrt((beta_o*gamma_o)/(beta_i*gamma_i))
            
            t11 = Mpart[0,0]*factor
            t12 = Mpart[0,1]*factor
            t21 = Mpart[1,0]*factor
            t22 = Mpart[1,1]*factor
            
            MTwiss = [[t11**2, -2*t11*t12, t12**2], [-t11*t21, t11*t22+t12*t21, -t12*t22], [t21**2, -2*t21*t22, t22**2]]
            
            Twiss_i = twiss_i[:,i]
            Twiss_i = np.reshape(Twiss_i, (3,1))
            #print("Twiss_i: " + str(Twiss_i))
            Twiss_f = MTwiss@Twiss_i
            #print("Twiss_f: " + str(Twiss_f))
            
            for j in range(3):
                twiss_o[j,i] = Twiss_f[j] #ugly solution but it did not want to cooperate
            
            # beta/gamma must always be above 0:
        if(twiss_o[0,0:2] < 0).any():
            raise ValueError(f"Calculated wrong Twiss matrix in column {i}: {twiss_o}")
        
    elif dim == 3:
        twiss_o = np.zeros((3,1))
        
        Mpart = M[4:6,4:6]
        
        #gamma_i = 1 + E_i/m0
        beta_i = np.sqrt(1-1/gamma_i**2)
        
        #gamma_o = 1 + E_o/m0
        beta_o = np.sqrt(1-1/gamma_o**2)
        
        factor = 1
            
        t11 = Mpart[0,0]*factor
        t12 = Mpart[0,1]*factor
        t21 = Mpart[1,0]*factor
        t22 = Mpart[1,1]*factor
        
        MTwiss = [[t11**2, -2*t11*t12, t12**2], [-t11*t21, t11*t22+t12*t21, -t12*t22], [t21**2, -2*t21*t22, t22**2]]
        
        #Twiss_i = twiss_i[:,i]
        Twiss_i = np.reshape(twiss_i, (3,1))
        #print("Twiss_i: " + str(Twiss_i))
        Twiss_f = MTwiss@Twiss_i
        #print("Twiss_f: " + str(Twiss_f))
            
        for j in range(3):
            twiss_o[j,0] = Twiss_f[j] #ugly solution but it did not want to cooperate

        # beta/gamma must always be above 0:
        if(twiss_o[0] < 0):
            raise ValueError(f"Calculated wrong Twiss matrix in column {i}: {twiss_o}")
    else:
        print("Wrong dim input into twissTrM_E1")

    return twiss_o

def Cavity_Matrix_Input_Phase_twiss_E1(elementType, Input_Phase, Input_Energy, AMP, Field_Map, twiss_i_0, z_i, dim):
    """ 
    A function that decribes the particles movement in an accelerating cavity, with the twiss parameters included.
    Calculated the energy in every step of the way.
    
    Input:
         - elementType: 1 = spoke, 2 = MBL and 3 = HBL
         - Input_Phase: defined by the lattice file (WHAT DOES THIS MEAN? Not defined by the abs_phase before?) [degrees]
         - Input_Energy: the energy of the particle enetring this cavity [MeV]
         - AMP: how much of the maximum power the cavity is running at, set in the lattice file.
         - Field_Map: the field_map of the relevant cavity running at the maximum power. Must be an array. [z (m), Ex (MV/m), Ey (MV/m), Ez (MV/m)]
         - twiss_i: a 3x2 matrix if dim = 12, and a 3x1 matrix if dim = 3, where each column is beta_i, alpha_i, gamma_i. And the three columns correpond to x and y  or only z.
         - z_i: the positon at the start of the cavity in m
         - dim: either 12 for transversal calculations or 3 for longitudinal calculations.
    
    Output:
         - Phase_Shift: The phase change of the beam as a result from travveling in the cavity [degrees]
         - Output_Energy: The energy of the beam a result of travelling in the cavity [MeV]
         - M: The matrix that descibes the cavities affect on the 6D phase-space of the beam [radian-m phase-space]
         - twiss_o = a 3x3 matrix where each column is beta_i, alpha_i, gamma_i. And the three columns correpond 
                to x,y and z. This matrix contains the initial values after exiting the cavity.
    
    """
    
    c = cnst.c
    m0 = cnst.m0
    
    if elementType == 1:
        f = cnst.f #Hz
    elif elementType == 2 or elementType == 3:
        f = 2*cnst.f
    else:
        print(f"Not a cavity as elementType input to Cavity_Matrix_Input_Phase_twiss_E1: {elementType}")
    
    Ez = Field_Map[:,3] # MV/m
    Ez = Ez*AMP
    z = Field_Map[:, 0] #m
    N = len(Ez)
    L_total = z[N-1]
    L = L_total/(N-1)
    
    In_Phase = Input_Phase*2*np.pi/360 #convert phase to radians
    gamma_i = 1 + Input_Energy/m0 #m0 defined in 'variables'
    beta_i = np.sqrt(1-1/gamma_i**2)
    
    Phase_Shift = 0
    
    Mx = np.identity(2)
    Mz = np.identity(2)
    
    if dim == 12:
        betas_element = np.zeros((N,3))
        twiss_o = twiss_i_0
        
        for i in range(0,N):
            gamma_o = gamma_i + Ez[i]*np.cos(In_Phase)*L/m0 # don't know where this comes from
            beta_o = np.sqrt(1-1/gamma_o**2)
            twiss_i = twiss_o
            
            #calculate dE (derivative of field times dL)
            if i == 0:
                dE = Ez[1]/2 #Ez is zero at the edges of the cavity
            elif i == N-1:
                dE = Ez[N-2]/2
            else:
                dE = (Ez[i+1]-Ez[i-1])/2
            
            k = 1/(gamma_i*(beta_i**2)*m0)
            
            #coefficients from matrix in longitudinal phase space
            m21zz = k*dE*np.cos(In_Phase)
            m22zz = 1 - k*Ez[i]*L*np.cos(In_Phase)
            
            #coefficients from matrix in tranversal phase space (Mxx=Myy)
            m21xx = k*(-0.5*dE*np.cos(In_Phase)+beta_i*c*2*np.pi*f/(2*c**2)*Ez[i]*L*np.sin(In_Phase))
            m22xx = 1-k*Ez[i]*L*np.cos(In_Phase)
            
            if i < N-1:
                
                Mz1 = [[1, L/gamma_o**2], [m21zz, m22zz]]
                
                Mx1 = [[1, L], [m21xx, m22xx]]
                
                M0 = np.array([[0,0],[0,0]])
                M1 = np.concatenate((Mx1, M0, M0), axis = 1)
                M2 = np.concatenate((M0, Mx1, M0), axis = 1)
                M3 = np.concatenate((M0, M0, Mz1), axis = 1)
                M = np.concatenate((M1, M2, M3), axis = 0)
                
                twiss_o = twissTrM_E1(twiss_i, M, gamma_i, gamma_o, dim)
                betas_element[i,1:] = twiss_o[0,:]
                betas_element[i,0] = z_i + (i+1)*L
            
            else:
                Mz1 = np.dot([[1, L/gamma_o**2], [m21zz, m22zz]], np.linalg.inv([[1, L/gamma_o**2], [0, 1]]))
                
                Mx1 = np.dot([[1, L], [m21xx, m22xx]], np.linalg.inv([[1, L], [0, 1]]))
                
                M0 = np.array([[0,0],[0,0]])
                M1 = np.concatenate((Mx1, M0, M0), axis = 1)
                M2 = np.concatenate((M0, Mx1, M0), axis = 1)
                M3 = np.concatenate((M0, M0, Mz1), axis = 1)
                M = np.concatenate((M1, M2, M3), axis = 0)
                
                twiss_o = twissTrM_E1(twiss_i, M, gamma_i, gamma_o, dim)
                betas_element[i,1:] = twiss_o[0,:]
                #print(f"betas_element[i,1:] = {betas_element[i,1:]}")
                betas_element[i,0] = z_i + (i+1)*L
                #print(f"betas_element[i,0] = {betas_element[i,0]}")
                
            Mz = np.dot(Mz1,Mz)
            Mx = np.dot(Mx1,Mx)
            
            d_Phy = 2*np.pi*f*L/(beta_o*c)
            In_Phase = In_Phase + d_Phy
            
            if i < N-1:
                Phase_Shift = Phase_Shift+d_Phy
            
            gamma_i = gamma_o
            beta_i = beta_o
            
            
    elif dim == 3:
        betas_element = np.zeros((N,2))
        twiss_o = twiss_i_0
        
        
        for i in range(0,N):
            gamma_o = gamma_i + Ez[i]*np.cos(In_Phase)*L/m0 # don't know where this comes from
            beta_o = np.sqrt(1-1/gamma_o**2)
            twiss_i = twiss_o
            
            #calculate dE (derivative of field times dL)
            if i == 0:
                dE = Ez[1]/2 #Ez is zero at the edges of the cavity
            elif i == N-1:
                dE = Ez[N-2]/2
            else:
                dE = (Ez[i+1]-Ez[i-1])/2
            
            k = 1/(gamma_i*(beta_i**2)*m0)
            
            #coefficients from matrix in longitudinal phase space
            m21zz = k*dE*np.cos(In_Phase)
            m22zz = 1 - k*Ez[i]*L*np.cos(In_Phase)
            
            #coefficients from matrix in tranversal phase space (Mxx=Myy)
            m21xx = k*(-0.5*dE*np.cos(In_Phase)+beta_i*c*2*np.pi*f/(2*c**2)*Ez[i]*L*np.sin(In_Phase))
            m22xx = 1-k*Ez[i]*L*np.cos(In_Phase)
            
            if i < N-1:
                
                Mz1 = [[1, L/gamma_o**2], [m21zz, m22zz]]
                
                Mx1 = [[1, L], [m21xx, m22xx]]
                
                M0 = np.array([[0,0],[0,0]])
                M1 = np.concatenate((Mx1, M0, M0), axis = 1)
                M2 = np.concatenate((M0, Mx1, M0), axis = 1)
                M3 = np.concatenate((M0, M0, Mz1), axis = 1)
                M = np.concatenate((M1, M2, M3), axis = 0)
                
                twiss_o = twissTrM_E1(twiss_i, M, gamma_i, gamma_o, dim)
                betas_element[i,1:] = twiss_o[0,:]
                betas_element[i,0] = z_i + (i+1)*L
            
            else:
                Mz1 = np.dot([[1, L/gamma_o**2], [m21zz, m22zz]], np.linalg.inv([[1, L/gamma_o**2], [0, 1]]))
                
                Mx1 = np.dot([[1, L], [m21xx, m22xx]], np.linalg.inv([[1, L], [0, 1]]))
                
                M0 = np.array([[0,0],[0,0]])
                M1 = np.concatenate((Mx1, M0, M0), axis = 1)
                M2 = np.concatenate((M0, Mx1, M0), axis = 1)
                M3 = np.concatenate((M0, M0, Mz1), axis = 1)
                M = np.concatenate((M1, M2, M3), axis = 0)
                
                twiss_o = twissTrM_E1(twiss_i, M, gamma_i, gamma_o, dim)
                betas_element[i,1] = twiss_o[0]
                #print(f"betas_element[i,1:] = {betas_element[i,1:]}")
                betas_element[i,0] = z_i + (i+1)*L
                #print(f"betas_element[i,0] = {betas_element[i,0]}")
                
            Mz = np.dot(Mz1,Mz)
            Mx = np.dot(Mx1,Mx)
            
            d_Phy = 2*np.pi*f*L/(beta_o*c)
            In_Phase = In_Phase + d_Phy
            
            if i < N-1:
                Phase_Shift = Phase_Shift+d_Phy
            
            gamma_i = gamma_o
            beta_i = beta_o
    else:
        print(f"Wrong dim input into Cavity_Matrix_Input_Phase_twiss_E1, dim = {dim}, should be 12 or 3")
    
    Output_Energy = (gamma_o-1)*m0 
    
    #convert Phase_shift to degrees
    if elementType == 1:
        Phase_Shift = Phase_Shift*360/(2*np.pi)
    else: #already checked to that elementType was 1,2 or 3, can therefore use only "else"
        Phase_Shift = Phase_Shift*360/(2*np.pi)/2
        
    My = Mx
    
    M0 = np.zeros((2,2))
    #M = np.array([[Mx, M0, M0], [M0, My, M0], [M0, M0, Mz]])
    M1 = np.concatenate((Mx, M0, M0), axis = 1)
    M2 = np.concatenate((M0, My, M0), axis = 1)
    M3 = np.concatenate((M0, M0, Mz), axis = 1)
    M = np.concatenate((M1, M2, M3), axis = 0)
    
    return Phase_Shift, Output_Energy, M, betas_element, twiss_o