#!/usr/bin/env python
# coding: utf-8

# In[102]:


import numpy as np
#import rematch_functions.ipynb as refunc
import Sofia_GA
import rematch_functions


# #               input  variables

# In[103]:


################################################################################
################# input from user, define the failed cavity ####################
################################################################################
cavType = 1 #1 for SPK, 2 for MBL and 3 for HBL
Num = 15 #in the interval [1,26] for SPK, [1,36] for MBL and [1,84] for HBL
latticeName = 'lattice.dat' #lattice file in traceWin fromat of accelerator
#newLatticeName = 'newLattice06032020.dat' # the name of the lattice file that will be generated
newLatticeName = 'newLatticeTest.dat' # for test runs



#################################################################################
#                           natural contants
#################################################################################
c = 2.99792458e8 #m/s, speed of light
m0 = 938.272029 #MeV, proton mass


################################################################################
#                            Cavity specifications
################################################################################
#field_maps for cavities
#will get all field_map values as an array of vectors (z, Ex, Ey, Ez)
FIELD_MAP_SPK=np.loadtxt('Field_Map/SPK_500.txt', usecols = (0,1,2,3), comments = '#', delimiter= ' ');
FIELD_MAP_MBL=np.loadtxt('Field_Map/MBL_500.txt', usecols = (0,1,2,3), comments = '#', delimiter= ' ');
FIELD_MAP_HBL=np.loadtxt('Field_Map/HBL_500.txt', usecols = (0,1,2,3), comments = '#', delimiter= ' '); 


#################################################################################
#                        Quadrupole specifications
#################################################################################
#lower and upper boundary of change fo cavity gradient (k1, k2, k3, k4)
lb_q = 0.8*np.ones(4)
ub_q = 1.1*np.ones(4)


#################################################################################
#                 input parameters for lattice
#################################################################################
#varibles from lattice
Input_Energy_0 = 89.887822 #MeV. Input energy of the first element

#cerate simplified lattice
simpleLatticeName = rematch_functions.readLattice(latticeName)

phaseFileName = rematch_functions.Ref_Phase(simpleLatticeName, Input_Energy_0, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL)

Ref_Phase = np.loadtxt(phaseFileName, usecols = (0,1,2,3,4,5,6,7,8,9), comments = '#', delimiter = ' ')

head, Lattice_Section_F, M_ref, Rematch_Cavities_index, margins = rematch_functions.list_z(cavType, Num, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, Ref_Phase)

##################################################################################
#             Input parameters for Genetic Algorithm
##################################################################################

###################  variables for genetic algorithm for cavity rematch  ################
nvars = 8 # number of variables in solution
pop_Size = 50
max_Gen = 100 #100
mut = 0.15
acr = 0.5
fit_Lim = -0.000001 # -0.001
#lower and upper boundaries for cavity settings (amp1, amp2, amp3, amp4, Input_Phase1, Input_Phase2, Input_Phase3, Input_Phase4)
lb_cav = np.concatenate((0.7*np.ones([1,4]), -50*np.ones([1,4])), axis = None)
ub_cav = np.concatenate((margins, 10*np.ones([1,4])), axis = None)

def fitness(x):
    """
    Defining a function  that will be the fitness function of the cavity rematch. It is the
    Error_z function but with only one variable.
    
    Input:
        - x = a possible solution in the shape of a vector with nvars elements.
        
    Output:
        - err = the error that the soultion x gives. Will return a value between 0 and infinity, 
                the closer to zero the better.
    """
    err = rematch_functions.Error_z(x, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, Lattice_Section_F, M_ref, head)
    return err


###################  variables for genetic algorithm for quad rematch  ###################
nvars_q = 4 # number of variables in solution
pop_Size_q = 200
max_Gen_q = 100 #100
fit_Lim_q = -0.001 #usually same as fit_Lim for caveties
#acr, mut and fit_Lim same as for the cavity rematch

def fitness_q(x):
    """
    Defining a function that will be the fotness function fo the Quadrupole rematch. It is the
    Error_t function but with only one variable.
    
    Input:
        - x = a possible solution in the hsape of a vector with nvars elements.
        
    Output:
        - err = the error that the soultion x gives. Will return a value between 0 and infinity, 
                the closer to zero the better.
    """
    err = rematch_functions.Error_t(x, Lattice_Section_Quads, Mt_ref, FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL)
    return err


# # Cavity rematch

# In[104]:


#################################################################################
#                           Cavity rematch
#################################################################################

print("Cavity rematch:")
opt_sol, opt_err = Sofia_GA.Sofia_myga(fitness, nvars, lb_cav, ub_cav, max_Gen, pop_Size, mut, acr , fit_Lim)



#################################################################################
#             Input_phase calculations for downstream cavities
#################################################################################

Ref_rephase = rematch_functions.Ref_RePhase(FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, head, Lattice_Section_F, Rematch_Cavities_index, opt_sol, Ref_Phase)


#################################################################################
#                               Quad rematch  
#################################################################################

print("Quadrupole rematch: ")

head_t, Lattice_Section_Quads, Mt_ref = rematch_functions.List_t(FIELD_MAP_SPK, FIELD_MAP_MBL, FIELD_MAP_HBL, head, Lattice_Section_F, Ref_rephase)
opt_sol_q, opt_err_q = Sofia_GA.Sofia_myga(fitness_q, nvars_q, lb_q, ub_q, max_Gen_q, pop_Size_q, mut, acr, fit_Lim_q)


#################################################################################
#                Put together the finished Lattice
#################################################################################

# change quad settings
Ref_rephase_quad =  rematch_functions.Ref_rephase_Quad(Ref_rephase, head_t, Lattice_Section_Quads, opt_sol_q)

# change type of failed cavity from 0 to what it was before
idx_f_temp = np.where(Rematch_Cavities_index[:,0] == 0)[0][0]
idx_f = Rematch_Cavities_index[idx_f_temp, 1]
print(Ref_rephase_quad[idx_f,:])
#Ref_rephase_quad[idx_f,0] = cavType
rematch_functions.lattice_update(Ref_rephase_quad, cavType, newLatticeName, latticeName)


# In[71]:


#test

a = 0

if a != 0:
    a = 10
    
print(a)


# In[ ]:




