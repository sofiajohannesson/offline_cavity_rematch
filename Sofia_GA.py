#!/usr/bin/env python
# coding: utf-8

# # a notebook containing a genetic algoritm to solve 
# # the minimization problem that is rematching of cavities

# In[264]:


#functions needed in the big GA

import numpy as np
from random import random
import math

def cal_pop_fitness(pop, pop_size, nvars, fittness_fun):
    """ Determines how good every possible solution in the population pop.
    
    Input:
        - pop: The current populations with possible solutions as rows. A matrix of size [pop_size, nvars]
        - pop_size: The number of possible solutions in pop.
        - nvars: the number fo variables in each solution
        - fitness_fun: a function thar determines the goodness of a solution. The input parameters will 
        be a set of variables a.k.a. one row of pop.
        
    Output:
        - fitness: a vector containing a fitness value for each possible solution in pop, 
        the order of the fitness values of the solutions is the same as the order or solutions in pop.
    """
    
    fitness = np.zeros(pop_size)
    for i in range(pop_size):
        x = np.zeros(nvars)
        for j in range(nvars):
            x[j] = pop[i,j]
        fitness[i] = -fittness_fun(x) #because we define the best value to be the highest value.
    return fitness

def FindBest(pop, fitness, nvars):
    """
    
    Input:
        - pop: the current set of solutions.
        - fitness: the fitness values for all solutions in pop
        - nvars: the number of variables in every solution
        
    Output:
        - sol_Best: A vector containing the best solution of population pop with the fitness value as 
                    the last value of the vector. This vector will have size = (1, nvars+1)
    """
    sol_Best = np.zeros(nvars+1) #one extra variable
    maxCorr = max(fitness)
    maxIdx = fitness.argmax()
    sol_Best[0:nvars] = pop[maxIdx, :]
    sol_Best[-1] = maxCorr
    
    return sol_Best
        
def select_mating_pool(pop_M_rm, fitness, num_parents):
    """ Selecting the num_parents best matrices from pop_M_rm using the values of fitness
    
    Input: 
        - pop_M_rm: a ndarray of potential rematch matrices
        - fitness: a vector containing value that indicate how good each matrix in pop_M_rm is.
                        The closer to zero the better
        - num_parents: number of parents to be chosen
        
    Output: 
        - parents: a ndarray containing the num_parents best matrices from pop_M_rm
    """
    # Selecting the best individuals in the current generation as parents for producing the offspring of the next generation.
    [pop_size, row, col] = pop_M_rm.shape
    parents = numpy.ndarray([num_parents, row, col])
    for parent_num in range(num_parents):
        max_fitness_idx = numpy.where(fitness == numpy.min(fitness))
        max_fitness_idx = max_fitness_idx[0][0] #because max_fitness will be an array-object and we want the value
        parents[parent_num, :, :] = pop_M_rm[max_fitness_idx, :, :]
        fitness[max_fitness_idx] = 99999999999 #so next time will be the next best one.
    return parents

def crossover(parents, num_offspring):
    """ mixes the parents a bit
    
    Input:
        - parents: ndarray containing a set of M_rm matrices
        - num_offspring: The number of children to be generated from the parents.
                                        Usually num_pop-num_parents
    
    Output:
        - offspring: a ndarray containing num_offpsring, gereated offspring matrices
    """
    #shifts halfs of the parents one step to mix the genes.
    [num_parents, row, col] = parents.shape
    offspring = numpy.ndarray([num_offspring, row, col])
    # The point at which crossover takes place between two parents. Usually, it is at the center.
    
    #for the mixing , perhaps i shoudl keeep the xx, yy and zz intact and switch arounf whole packages of them.
    #so that x' settings are only switched with other x' or y' and z' only with z'? or is this ruining the algorithm?
    crossover_point = numpy.uint8(offspring_size[1]/2)
    
    #switch arund xx' yy'

    for k in range(offspring_size[0]):
        # Index of the first parent to mate.
        parent1_idx = k%parents.shape[0]
        # Index of the second parent to mate.
        parent2_idx = (k+1)%parents.shape[0]
        # The new offspring will have its first half of its genes taken from the first parent.
        offspring[k, 0:crossover_point] = parents[parent1_idx, 0:crossover_point]
        # The new offspring will have its second half of its genes taken from the second parent.
        offspring[k, crossover_point:] = parents[parent2_idx, crossover_point:]
    return offspring

def mutation(pop, mut, pop_size, nvars, var_range, t, num_gen):
    """ A function that remakes pop  to have mutations
    
    Inputs:
        - pop: the last population
        - mut:  value between 0 and 1 indicating how much mutaions will be made
        - pop_size: The number of suggested solutions in pop
        - nvars: the number of variables that go into the solution
        - var_range: the allowed values of each values as a (2xnvars) matrix, 
        where the top row is the lower boundary for each variable and the bottom line 
        is the upper boundaries.
        - t: what generation we are on (so it can vary less for higher generations)
        - num_gen: top number of generations.
        
    Output:
        - pop_new: a mutated version of the input population (same shape)
    """
    for i in range(pop_size): #going though each possible solution in the populations
        for j in range(nvars): #going though each variable in a solution
            mut_rand = random()
            if mut_rand <= mut:
                mut_pm = random() #plus or minus
                mut_num = random()*(1-t/num_gen)**2
                tempValue = pop[i,j]
                if mut_pm <= 0.5: #minus
                    tempValue = tempValue*(1-mut_num)
                else: #plus
                    tempValue = tempValue*(1+mut_num)
                #check if the value is within boundaries
                temp = IfOut(tempValue, var_range[:,j])
                pop[i,j] = temp
    pop_new = pop
    
    return pop_new
    
def IfOut(c, c_range):
    """ A function that checks if value c is outside c_range.
    If it is less than its lower boundary it is set to the lowest value.
    If it is more than its upper boundary it is det to the highest value.
    Else the same value is returned.
    
    Input:
        - c: the value to be checked
        - c_range: A vector consisting of [lower boundary, upper boundary]
        
    Output:
        - new_c: the new value of c.
    """
    if c < c_range[0]:
        c_new = c_range[0]
    elif c > c_range[1]:
        c_new = c_range[1]
    else:
        c_new = c
        
    return c_new
        
def AcrChrom(pop, acr, pop_Size, nvars):
    """ A function that switches a few variables around between solutions. However variable 1 
        in a solution is only switched with other variables 1 in different solutions, same goes 
        for all variables.
    
    Input:
        - pop: the current population of solutions. Each row is a 
                vector containing possible values of the variables.
        - arc: a number in the range [0,1] that determines how much the populations is to be changed by this function
        - pop_Size: the number of solutions in pop
        - nvars: the number of variables in each solution
        
    Output:
        - pop_new: a new set of solutions slightly different from pop, but with the same size.
    """
    for i in range(pop_Size):
        acr_rand = random()
        if acr_rand < acr:
            acr_sol = math.floor((pop_Size-1)*random())#one of the solutions in the population
            acr_node = math.floor((nvars-1)*random())#one of the variables in the chosen solution
            temp = pop[i, acr_node]
            pop[i, acr_node] = pop[acr_sol, acr_node]
            pop[acr_sol, acr_node] = temp
    pop_new = pop
    
    return pop_new

def replace_Worse(pop, sol_Best, fitness, pop_Size):
    """ A function that replaces all solutions with a fitnessvalue less 
    than a certain value with the best solution.
    
    Input: 
        - pop: the current population of solutions. Each row is a 
                vector containing possible values of the variables.
        - sol_best: a vector containing the variables of the best solution ending with
                     the fitness value of that same solution.
        - fitness: the fitness values for all solutions in pop
        - pop_Size: the number of solutions in pop
        
    Output:
        - pop_new: a new set of solutions where all of the solutions have a fitness value better than 
                     limit = (max_num-min_num)*0.2 + min_num
        - fitness_new: the fitness values to the solution in pop_new
    """
    max_num = max(fitness)
    min_num = min(fitness)
    limit = (max_num-min_num)*0.2 + min_num
    
    #replace all solutions w a fitness level less than limit eith the best solution
    for i in range(pop_Size):
        if fitness[i] < limit:
            pop[i, :] = sol_Best[0:-1]
            fitness[i] = sol_Best[-1]
    
    pop_new = pop # feels somewhat unnessesary
    fitness_new = fitness #same
    return pop, fitness


# # the main genetic algoritm that uses the functions defined above

# In[271]:


def Sofia_myga(func, nvars, lb, ub, max_Gen, pop_Size, mut = 0.15, acr = 0.5, fit_Lim = -0.001):
    """ A function that calculated the best solution.
    
    Input:
        - func: a function that determines a fitnesscalue for a solution x. The input to this function
                is a vector containing a complete set of variables for a possible solution.
        - nvars: the number of variables in a solution.
        - lb: a vector containing the lower boundary of each variable
        - ub: a vector containing the upper boundary of each variable
        - max_Gen: The maximum number of generations, i.e. the max number of times 
                    a new set of solutions are generated.
        - pop_Size: the number of soultions in every generation.
        - mut: a value between 0 and 1 that determines how many mutations occur in the creation
                of a new generation. The closer to 1, the more mutations. Default value is 0.15.
        - acr: a value between 0 and 1 that determines how much the solutions are mixed with each other.
                Default value if 0.5.
        - fitness_limit: The fitness value where no new generations are calculated. It is consedered good enough
        
    Output:
        - opt_sol: the solution with the best fitness value
        - opt_fitness: the fitness value of opt_sol
    """
    
    #sol_range = np.concatenate((lb, ub), axis = 0)
    sol_range = np.vstack((lb, ub))
    fitness_ave = np.zeros(max_Gen, dtype=float) #(average)
    fitness_best = np.zeros(max_Gen, dtype=float) # a place to store the best fitness value of every generation
    
    diffs = np.subtract(ub,lb) #ub-lb
    diffsMatrix =  np.diag(diffs) #matrix with the diffs for index i at position [i,i]. Off-diagonal elements are zero
    offsets = np.multiply(np.ones((pop_Size, 1)), lb)
    
    pop = np.random.rand(pop_Size, nvars)
    pop = np.dot(pop, diffsMatrix)
    pop = np.add(offsets, pop) #to get the values within the correct range
    fitness = cal_pop_fitness(pop, pop_Size, nvars, func)
    sol_Best = FindBest(pop, fitness, nvars)
    fitness_best[0] = sol_Best[-1]
    fitness_ave[0] = sum(fitness)/pop_Size
    #should I do a replace worse here also?
    
    t = 0
    while t< max_Gen and sol_Best[-1] < fit_Lim:
        print("Generation: " + str(t))
        pop = mutation(pop, mut, pop_Size, nvars, sol_range, t, max_Gen) #mutate the solutions
        pop = AcrChrom(pop, acr, pop_Size, nvars) #switch some variables around between solutions
        fitness = cal_pop_fitness(pop, pop_Size, nvars, func)
        sol_Best_Temp = FindBest(pop, fitness, nvars)
        if sol_Best_Temp[-1] > sol_Best[-1]:
            sol_Best = sol_Best_Temp
        pop, fitness =  replace_Worse(pop, sol_Best, fitness, pop_Size)
        fitness_best[t] = sol_Best[-1]
        print("fitness value: " + str(sol_Best[-1]))
        fitness_ave[t] = sum(fitness)/pop_Size
        t += 1
    
    print("number of generations: " + str(t))
    opt_sol = sol_Best[0:-1]
    opt_fitness = sol_Best[-1]
    
    return opt_sol, opt_fitness


# In[270]:


# The difference between dot, matmul och multiply

#A = [1,2,3,4]
#AA = [1,5,2,9]

#print(np.concatenate((A,AA), axis = 1))


#B = [[1], [2], [3], [4]]

#C = [[1,2], [3,4]]
#D = [[1, 0], [1,1]]

#print(D.shape())

#E = [5, 6]
#F = [[7],[8]]

#print("dot")
#print(np.dot(B, A))
#print(np.dot(D,C))
#print(np.dot(E,C))
#print(np.dot(C,F))
#
#print("multiply") #this is the one I wanna use
#print(np.multiply(B, A))
#print(np.multiply(D,C))
#print(np.multiply(E,C))
#print(np.multiply(C,F))
#
#print("matmul")
#print(np.matmul(B,A))
#print(np.matmul(D,C))
#print(np.matmul(E,C))
#print(np.matmul(C,F))


# In[ ]:




